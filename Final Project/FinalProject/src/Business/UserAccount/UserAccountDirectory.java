/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Employee.Employee;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class UserAccountDirectory {
    private ArrayList<UserAccount> listOfUserAccounts;

    public UserAccountDirectory() {
        listOfUserAccounts = new ArrayList<>();
    }

    public ArrayList<UserAccount> getListOfUserAccounts() {
        return listOfUserAccounts;
    }
    
    public UserAccount authenticateUserAccount(String username, String password) {
        for(UserAccount ua : listOfUserAccounts) {
            if(ua.getUsername().equals(username) && ua.getPassword().equals(password)) {
                return ua;
            }
        }
        return null;
    }
    
    public UserAccount createUserAccount(String username, String password, Employee employee, Role role) {
        UserAccount ua = new UserAccount();
        ua.setUsername(username);
        ua.setPassword(password);
        ua.setEmployee(employee);
        ua.setRole(role);
        listOfUserAccounts.add(ua);
        return ua;
    }
    
    public boolean checkIfUaUnique(String username) {
        for(UserAccount ua : listOfUserAccounts) {
            if(ua.getUsername().equals(username)) {
                return false;
            }
        }
        return true;
    }
    public void deleteUserAccount(UserAccount u) {
        listOfUserAccounts.remove(u);
    }
    
}
