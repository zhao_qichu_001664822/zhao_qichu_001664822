/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author ZappyZhao
 */
public abstract class Role {
    
    public enum RoleType {
        HealthAdmin("Health Admin"),
        EPAAdmin("EPA Admin"),
        Doctor("Doctor"),
        HealthOfficial("Health Official"),
        Mayor("Mayor"),
        Investigator("Investigator"),
        User("User"),
        EPAOfficial("EPA Official");
        
        private String value;

        private RoleType(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }              
    }
    
    public abstract JPanel createWorkArea(JPanel container, 
            UserAccount userAccount, 
            Organization organization,
            Enterprise enterprise,
            EcoSystem system);

    @Override
    public String toString() {
        return this.getClass().getName();
    }
    
    
}
