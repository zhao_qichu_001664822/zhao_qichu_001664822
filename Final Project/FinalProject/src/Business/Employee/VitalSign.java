/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.Date;

/**
 *
 * @author ZappyZhao
 */
public class VitalSign {
    private int respiratoryRate;
    private int heartRate;
    private float bloodPressure;
    private float weight;
    private Date date;
    private String status;
    private String coughDegree;
    private String smokeDegree;
    private float bodyTemp;

    public int getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(int respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public float getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(float bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCoughDegree() {
        return coughDegree;
    }

    public void setCoughDegree(String coughDegree) {
        this.coughDegree = coughDegree;
    }

    public String getSmokeDegree() {
        return smokeDegree;
    }

    public void setSmokeDegree(String smokeDegree) {
        this.smokeDegree = smokeDegree;
    }

    public float getBodyTemp() {
        return bodyTemp;
    }

    public void setBodyTemp(float bodyTemp) {
        this.bodyTemp = bodyTemp;
    }

    @Override
    public String toString() {
        return status;
    }

    
    
    
}
