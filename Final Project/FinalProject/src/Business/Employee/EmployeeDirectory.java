/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class EmployeeDirectory {
    private ArrayList<Employee> listOfEmployees;

    public EmployeeDirectory() {
        listOfEmployees = new ArrayList<>();
    }

    public ArrayList<Employee> getListOfEmployees() {
        return listOfEmployees;
    }
    
    public Employee createEmployee(String name) {
        Employee employee = new Employee();
        employee.setName(name);
        listOfEmployees.add(employee);
        return employee;
    }
    
    public void deleteEmployee(Employee e) {
        listOfEmployees.remove(e);
    }
    
}
