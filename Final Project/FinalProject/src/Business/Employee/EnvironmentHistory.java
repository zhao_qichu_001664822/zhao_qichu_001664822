/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class EnvironmentHistory {
    private ArrayList<Environment> listOfEnvironment;

    public EnvironmentHistory() {
        listOfEnvironment = new ArrayList<>();
    }

    public ArrayList<Environment> getListOfEnvironment() {
        return listOfEnvironment;
    }
    
    public Environment addNewEnvironment() {
        Environment e = new Environment();
        listOfEnvironment.add(e);
        return e;
    }
    
}
