/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class SensorDirectory {
    private ArrayList<Sensor> listOfSensors;

    public SensorDirectory() {
        listOfSensors = new ArrayList<>();
        
    }

    public ArrayList<Sensor> getListOfSensors() {
        return listOfSensors;
    }
    
    public Sensor addSensor() {
        Sensor s = new Sensor();
        listOfSensors.add(s);
        return s;
        
    }
}
