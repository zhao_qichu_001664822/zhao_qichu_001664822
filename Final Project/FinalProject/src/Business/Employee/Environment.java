/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.Date;

/**
 *
 * @author ZappyZhao
 */
public class Environment {
    private float temperature;
    private float humidity;
    private float ozonePollution;
    private float particlePollution;
    private String status;
    private Date date;

    public float getTemperature() {
        return temperature;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getOzonePollution() {
        return ozonePollution;
    }

    public void setOzonePollution(float ozonePollution) {
        this.ozonePollution = ozonePollution;
    }

    public float getParticlePollution() {
        return particlePollution;
    }

    public void setParticlePollution(float particlePollution) {
        this.particlePollution = particlePollution;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
