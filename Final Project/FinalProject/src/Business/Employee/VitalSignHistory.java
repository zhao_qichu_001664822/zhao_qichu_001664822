/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class VitalSignHistory {
    private ArrayList<VitalSign> vitalSignList;
    public VitalSignHistory(){
        vitalSignList = new ArrayList<VitalSign>();
}

    public ArrayList<VitalSign> getVitalSignList() {
        return vitalSignList;
    }
    
    public VitalSign addVitalSign(){
        VitalSign vs = new VitalSign();
        vitalSignList.add(vs);
        return vs;
    }
}
