/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

/**
 *
 * @author ZappyZhao
 */
public class Sensor {
    private static int count=1000;
    private int sensorId;
    private String address;
    private String community;
    private EnvironmentHistory environmentHistory;

    public Sensor() {
        sensorId=count;
        count++;
        environmentHistory = new EnvironmentHistory();
    }

    public EnvironmentHistory getEnvironmentHistory() {
        return environmentHistory;
    }

    public void setEnvironmentHistory(EnvironmentHistory environmentHistory) {
        this.environmentHistory = environmentHistory;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }
    
    
}
