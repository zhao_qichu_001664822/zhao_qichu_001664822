/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author ZappyZhao
 */
public class OfficialWorkRequest extends WorkRequest {
    private String abnormalPercentage;
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getAbnormalPercentage() {
        return abnormalPercentage;
    }

    public void setAbnormalPercentage(String abnormalPercentage) {
        this.abnormalPercentage = abnormalPercentage;
    }
}
