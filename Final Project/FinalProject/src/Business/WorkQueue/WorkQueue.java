/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class WorkQueue {
    private ArrayList<WorkRequest> listOfWorkRequests;

    public WorkQueue() {
        listOfWorkRequests = new ArrayList<>();
    }

    public ArrayList<WorkRequest> getListOfWorkRequests() {
        return listOfWorkRequests;
    }
    
}
