/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.EPAOfficialRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class EPAOfficeOrganization extends Organization {

    public EPAOfficeOrganization() {
        super(Type.EPAOffice.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> r = new ArrayList<>();
        r.add(new EPAOfficialRole());
        return r;
    }
    
}
