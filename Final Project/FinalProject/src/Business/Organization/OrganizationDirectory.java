/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.EcoSystem;
import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class OrganizationDirectory {
    private ArrayList<Organization> organizationList;
    
    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type) {
        Organization organization = null;
        if(type.getValue().equals(Type.HealthAdmin.getValue())) {
            organization = new HealthAdminOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.EPAAdmin.getValue())) {
            organization = new EPAAdminOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.EPAOffice.getValue())) {
            organization = new EPAOfficeOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.Environment.getValue())) {
            organization = new EnvironmentOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.HealthOffice.getValue())) {
            organization = new HealthOfficeOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.Mayor.getValue())) {
            organization = new MayorOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.User.getValue())) {
            organization = new UserOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.VitalSign.getValue())) {
            organization = new VitalSignOrganization();
            organizationList.add(organization);
        }
        
        return organization;
    }
}
