/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.GovernmentAdminRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class GovernmentAdminOrganization extends Organization {

    public GovernmentAdminOrganization() {
        super(Type.GovernmentAdmin.getValue());
    }

    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> r = new ArrayList<>();
        r.add(new GovernmentAdminRole());
        return r;
    }
    
}
