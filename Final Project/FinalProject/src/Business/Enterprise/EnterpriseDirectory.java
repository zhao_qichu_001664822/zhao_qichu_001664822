/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class EnterpriseDirectory {
    private ArrayList<Enterprise> listOfEnterprises;

    public EnterpriseDirectory() {
        listOfEnterprises = new ArrayList<>();
    }

    public ArrayList<Enterprise> getListOfEnterprises() {
        return listOfEnterprises;
    }
    
    public Enterprise createEnterprise(String name, Enterprise.EnterpriseType type) {
        Enterprise enterprise = null;
        if(type == Enterprise.EnterpriseType.HealthCareCenter) {
            enterprise = new HealthCareCenterEnterprise(name);
            listOfEnterprises.add(enterprise);
        } 
        else if(type == Enterprise.EnterpriseType.Government) {
            enterprise = new GovernmentEnterprise(name);
            listOfEnterprises.add(enterprise);
        }
        else if(type == Enterprise.EnterpriseType.EPA) {
            enterprise = new EPAEnterprise(name);
            listOfEnterprises.add(enterprise);
        }
        return enterprise;
    }
    
    public void deleteEnterprise(Enterprise e) {
        listOfEnterprises.remove(e);
    }
    
}
