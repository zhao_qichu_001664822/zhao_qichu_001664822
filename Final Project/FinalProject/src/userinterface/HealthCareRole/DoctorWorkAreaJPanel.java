/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.HealthCareRole;

import Business.Employee.Employee;
import Business.Employee.VitalSign;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.JPanel;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ZappyZhao
 */
public class DoctorWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Enterprise enterprise;

    DefaultTableModel dtm;
    /**
     * Creates new form DotorWorkAreaJPanel
     */
    public DoctorWorkAreaJPanel(JPanel userProcessContainer, UserAccount userAccount, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = userAccount;
        this.enterprise = enterprise;
        backgroundjLabel.setSize(1000, 1000);
        
        doctorNamejLabel.setText(userAccount.getEmployee().getName());
        popCommunity();
        
        
    }

    public void popCommunity() {
        communityjComboBox.removeAllItems();
        communityjComboBox.addItem("Los Angeles");
        communityjComboBox.addItem("San Diego");
        communityjComboBox.addItem("Orange");
    }

    public void popTableLA() {
        //System.out.println("aaaaa");
        DateFormat dateFormat = new SimpleDateFormat("HH:mm MM/dd/yyyy");
        dtm = (DefaultTableModel)vitalSignjTable.getModel();
        dtm.setRowCount(0);
        //Check community is LA or not
        for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if(organization.getName().equals("User Organization")) {
                for(Employee employee : organization.getEmployeeDirectory().getListOfEmployees()) {
                    //System.out.println(employee.getSensor().getCommunity());
                    try {
                        
                    if (employee.getSensor().getCommunity().equals("Los Angeles")) {
                        for (VitalSign vs : employee.getVitalSignHistory().getVitalSignList()) {
                            Object row[] = new Object[10];
                            row[0] = employee.getName();
                            row[1] = dateFormat.format(vs.getDate());
                            row[2] = vs;
                            row[3] = vs.getBloodPressure();
                            row[4] = vs.getRespiratoryRate();
                            row[5] = vs.getHeartRate();
                            row[6] = vs.getWeight();
                            row[7] = vs.getSmokeDegree();
                            row[8] = vs.getCoughDegree();
                            row[9] = vs.getBodyTemp();
                            dtm.addRow(row);
                        }
                    }
                    } catch(Exception e) {
                        return;
                    }
                }
            }
        }
    }
    
    public void popTableSD() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm MM/dd/yyyy");
        dtm = (DefaultTableModel)vitalSignjTable.getModel();
        dtm.setRowCount(0);
        //Check community is San Diego or not
        for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if(organization.getName().equals("User Organization")) {
                for(Employee employee : organization.getEmployeeDirectory().getListOfEmployees()) {
                    try {
                     
                    if (employee.getSensor().getCommunity().equals("San Diego")) {
                        for (VitalSign vs : employee.getVitalSignHistory().getVitalSignList()) {
                            Object row[] = new Object[10];
                            row[0] = employee.getName();
                            row[1] = dateFormat.format(vs.getDate());
                            row[2] = vs;
                            row[3] = vs.getBloodPressure();
                            row[4] = vs.getRespiratoryRate();
                            row[5] = vs.getHeartRate();
                            row[6] = vs.getWeight();
                            row[7] = vs.getSmokeDegree();
                            row[8] = vs.getCoughDegree();
                            row[9] = vs.getBodyTemp();
                            dtm.addRow(row);
                        }
                    }
                    } catch(Exception e) {
                        return;
                    }
                }
            }
        }
    }
    
    public void popTableO() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm MM/dd/yyyy");
        dtm = (DefaultTableModel)vitalSignjTable.getModel();
        dtm.setRowCount(0);
        //Check community is Orange or not
        for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if(organization.getName().equals("User Organization")) {
                for(Employee employee : organization.getEmployeeDirectory().getListOfEmployees()) {
                    try {
                     
                    if (employee.getSensor().getCommunity().equals("Orange")) {
                        for (VitalSign vs : employee.getVitalSignHistory().getVitalSignList()) {
                            Object row[] = new Object[10];
                            row[0] = employee.getName();
                            row[1] = dateFormat.format(vs.getDate());
                            row[2] = vs;
                            row[3] = vs.getBloodPressure();
                            row[4] = vs.getRespiratoryRate();
                            row[5] = vs.getHeartRate();
                            row[6] = vs.getWeight();
                            row[7] = vs.getSmokeDegree();
                            row[8] = vs.getCoughDegree();
                            row[9] = vs.getBodyTemp();
                            dtm.addRow(row);
                        }
                    }
                    } catch(Exception e) {
                        return;
                    }
                }
            }
        }
    }
    
    public void filter(String query) {
        //System.out.println("aaaaaaaaaa");
        //DefaultTableModel dtm = (DefaultTableModel)vitalSignjTable.getModel();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(dtm);
        vitalSignjTable.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(query));
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        doctorNamejLabel = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        communityjComboBox = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        vitalSignjTable = new javax.swing.JTable();
        getAbPercjButton = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        abnormalPercjLabel = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        filterDatejTextField = new javax.swing.JTextField();
        sendjButton = new javax.swing.JButton();
        backgroundjLabel = new javax.swing.JLabel();

        setLayout(null);

        jLabel1.setFont(new java.awt.Font("Lao MN", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Doctor Work Area");
        add(jLabel1);
        jLabel1.setBounds(15, 16, 140, 20);

        jLabel2.setFont(new java.awt.Font("Lao MN", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Doctor Name:");
        add(jLabel2);
        jLabel2.setBounds(155, 16, 120, 20);

        doctorNamejLabel.setFont(new java.awt.Font("Lao MN", 1, 13)); // NOI18N
        doctorNamejLabel.setText("jLabel3");
        add(doctorNamejLabel);
        doctorNamejLabel.setBounds(275, 16, 80, 19);

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Choose community to view Vital Sign:");
        add(jLabel3);
        jLabel3.setBounds(15, 49, 238, 16);

        communityjComboBox.setBackground(new java.awt.Color(255, 255, 255));
        communityjComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        communityjComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                communityjComboBoxActionPerformed(evt);
            }
        });
        add(communityjComboBox);
        communityjComboBox.setBounds(281, 45, 129, 27);

        vitalSignjTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "User Name", "Date", "Status", "Blood Pressure", "Respiratory Rate", "Heat Rate", "Weight", "Smoke Degree", "Cough Degree", "Body Temperature"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(vitalSignjTable);

        add(jScrollPane1);
        jScrollPane1.setBounds(6, 84, 830, 262);

        getAbPercjButton.setBackground(new java.awt.Color(255, 255, 255));
        getAbPercjButton.setFont(new java.awt.Font("Lao MN", 1, 13)); // NOI18N
        getAbPercjButton.setText("Get Abnormal Percentage");
        getAbPercjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getAbPercjButtonActionPerformed(evt);
            }
        });
        add(getAbPercjButton);
        getAbPercjButton.setBounds(6, 364, 215, 29);

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Abnormal Percentage at This Time:");
        add(jLabel4);
        jLabel4.setBounds(220, 369, 219, 16);

        abnormalPercjLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        abnormalPercjLabel.setForeground(new java.awt.Color(255, 255, 255));
        abnormalPercjLabel.setText("%");
        add(abnormalPercjLabel);
        abnormalPercjLabel.setBounds(457, 369, 86, 16);

        jLabel5.setText("Filter:");
        add(jLabel5);
        jLabel5.setBounds(441, 49, 36, 16);

        filterDatejTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                filterDatejTextFieldKeyReleased(evt);
            }
        });
        add(filterDatejTextField);
        filterDatejTextField.setBounds(495, 44, 179, 26);

        sendjButton.setBackground(new java.awt.Color(255, 255, 255));
        sendjButton.setFont(new java.awt.Font("Lao MN", 1, 13)); // NOI18N
        sendjButton.setText("Message Box with Health Official");
        sendjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendjButtonActionPerformed(evt);
            }
        });
        add(sendjButton);
        sendjButton.setBounds(585, 364, 263, 29);

        backgroundjLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/background.jpg"))); // NOI18N
        add(backgroundjLabel);
        backgroundjLabel.setBounds(0, 0, 680, 540);
    }// </editor-fold>//GEN-END:initComponents

    private void communityjComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_communityjComboBoxActionPerformed
        // TODO add your handling code here:
        String selectedCommunity = String.valueOf(communityjComboBox.getSelectedItem());
        if(selectedCommunity == "Los Angeles") {
            
            popTableLA();
        } else if(selectedCommunity == "San Diego") {
            
            popTableSD();
        } else if(selectedCommunity == "Orange") {
            
            popTableO();
        }
        
    }//GEN-LAST:event_communityjComboBoxActionPerformed

    private void filterDatejTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filterDatejTextFieldKeyReleased
        // TODO add your handling code here:
        String query = filterDatejTextField.getText();
        filter(query);
        
    }//GEN-LAST:event_filterDatejTextFieldKeyReleased

    private void getAbPercjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getAbPercjButtonActionPerformed
        double percent;
        int abnormalCounter=0;
        for(int i=0; i<vitalSignjTable.getRowCount();i++) {
            if(String.valueOf(vitalSignjTable.getValueAt(i, 2)).equals("Abnormal")) {
        //System.out.println(vitalSignjTable.getValueAt(i, 2));
                abnormalCounter++;
            }
        }
        percent = (double)abnormalCounter/(double)(vitalSignjTable.getRowCount())*100;
        abnormalPercjLabel.setText(String.format("%.2f", percent)+"%");
        
        //System.out.println(abnormalCounter);
        
    }//GEN-LAST:event_getAbPercjButtonActionPerformed

    private void sendjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendjButtonActionPerformed
        DToHOJPanel dtho = new DToHOJPanel(userProcessContainer, userAccount, enterprise, abnormalPercjLabel.getText());
        CardLayout cardlayout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("Doctor to Health Official JPanel", dtho);
        cardlayout.next(userProcessContainer);
    }//GEN-LAST:event_sendjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel abnormalPercjLabel;
    private javax.swing.JLabel backgroundjLabel;
    private javax.swing.JComboBox communityjComboBox;
    private javax.swing.JLabel doctorNamejLabel;
    private javax.swing.JTextField filterDatejTextField;
    private javax.swing.JButton getAbPercjButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton sendjButton;
    private javax.swing.JTable vitalSignjTable;
    // End of variables declaration//GEN-END:variables
}
