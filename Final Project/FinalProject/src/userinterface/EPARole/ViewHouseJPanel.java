/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.EPARole;

import Business.EcoSystem;
import Business.Employee.Employee;
import Business.Employee.Environment;
import Business.Enterprise.Enterprise;
import Business.Enterprise.HealthCareCenterEnterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Organization.UserOrganization;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ZappyZhao
 */
public class ViewHouseJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Enterprise enterprise;
    private EcoSystem system;
    DefaultTableModel dtm;

    private String date;
    private String houseStatus;
    private String temperature;
    private String humidity;
    private String ozone;
    private String pm;
    private String address;
    private String username;

    /**
     * Creates new form ViewHouseJPanel
     */
    public ViewHouseJPanel(JPanel userProcessContainer, UserAccount userAccount, Enterprise enterprise, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = userAccount;
        this.enterprise = enterprise;
        this.system = system;
        backgroundjLabel.setSize(1000, 1000);
        popCommunity();

        namejLabel.setText(userAccount.getEmployee().getName());
    }

    public void popCommunity() {
        communityjComboBox.removeAllItems();
        communityjComboBox.addItem("Los Angeles");
        communityjComboBox.addItem("San Diego");
        communityjComboBox.addItem("Orange");
    }

    public void popTableLA() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm MM/dd/yyyy");
        dtm = (DefaultTableModel) environmentjTable.getModel();
        dtm.setRowCount(0);

        for (Network network : system.getNetworkList()) {
            for (Enterprise ent : network.getEnterpriseDirectory().getListOfEnterprises()) {
                if (ent instanceof HealthCareCenterEnterprise) {
                    for (Organization org : ent.getOrganizationDirectory().getOrganizationList()) {
                        if (org instanceof UserOrganization) {
                            for (Employee employee : org.getEmployeeDirectory().getListOfEmployees()) {
                                try {
                                    if (employee.getSensor().getCommunity().equals("Los Angeles")) {
                                        for (Environment environment : employee.getSensor().getEnvironmentHistory().getListOfEnvironment()) {
                                            Object row[] = new Object[8];
                                            row[0] = dateFormat.format(environment.getDate());
                                            row[1] = environment.getStatus();
                                            row[2] = environment.getTemperature();
                                            row[3] = environment.getHumidity();
                                            row[4] = environment.getOzonePollution();
                                            row[5] = environment.getParticlePollution();
                                            row[6] = employee.getSensor().getAddress();
                                            row[7] = employee.getName();
                                            dtm.insertRow(0, row);
                                        }
                                    }
                                } catch (Exception e) {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //System.out.println(ent.getName());

    public void popTableSD() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm MM/dd/yyyy");
        dtm = (DefaultTableModel) environmentjTable.getModel();
        dtm.setRowCount(0);

        for (Network network : system.getNetworkList()) {
            for (Enterprise ent : network.getEnterpriseDirectory().getListOfEnterprises()) {
                if (ent instanceof HealthCareCenterEnterprise) {
                    for (Organization org : ent.getOrganizationDirectory().getOrganizationList()) {
                        if (org instanceof UserOrganization) {
                            for (Employee employee : org.getEmployeeDirectory().getListOfEmployees()) {
                                try {
                                    if (employee.getSensor().getCommunity().equals("San Diego")) {
                                        for (Environment environment : employee.getSensor().getEnvironmentHistory().getListOfEnvironment()) {
                                            Object row[] = new Object[8];
                                            row[0] = dateFormat.format(environment.getDate());
                                            row[1] = environment.getStatus();
                                            row[2] = environment.getTemperature();
                                            row[3] = environment.getHumidity();
                                            row[4] = environment.getOzonePollution();
                                            row[5] = environment.getParticlePollution();
                                            row[6] = employee.getSensor().getAddress();
                                            row[7] = employee.getName();
                                            dtm.insertRow(0, row);
                                        }
                                    }
                                } catch (Exception e) {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void popTableO() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm MM/dd/yyyy");
        dtm = (DefaultTableModel) environmentjTable.getModel();
        dtm.setRowCount(0);

        for (Network network : system.getNetworkList()) {
            for (Enterprise ent : network.getEnterpriseDirectory().getListOfEnterprises()) {
                if (ent instanceof HealthCareCenterEnterprise) {
                    for (Organization org : ent.getOrganizationDirectory().getOrganizationList()) {
                        if (org instanceof UserOrganization) {
                            for (Employee employee : org.getEmployeeDirectory().getListOfEmployees()) {
                                try {
                                    if (employee.getSensor().getCommunity().equals("Orange")) {
                                        for (Environment environment : employee.getSensor().getEnvironmentHistory().getListOfEnvironment()) {
                                            Object row[] = new Object[8];
                                            row[0] = dateFormat.format(environment.getDate());
                                            row[1] = environment.getStatus();
                                            row[2] = environment.getTemperature();
                                            row[3] = environment.getHumidity();
                                            row[4] = environment.getOzonePollution();
                                            row[5] = environment.getParticlePollution();
                                            row[6] = employee.getSensor().getAddress();
                                            row[7] = employee.getName();
                                            dtm.insertRow(0, row);
                                        }
                                    }
                                } catch (Exception e) {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void filter(String query) {
        //System.out.println("aaaaaaaaaa");
        //DefaultTableModel dtm = (DefaultTableModel)vitalSignjTable.getModel();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(dtm);
        environmentjTable.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(query));

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        namejLabel = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        communityjComboBox = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        environmentjTable = new javax.swing.JTable();
        backjButton5 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        filterDatejTextField = new javax.swing.JTextField();
        sendjButton = new javax.swing.JButton();
        checkjButton = new javax.swing.JButton();
        backgroundjLabel = new javax.swing.JLabel();

        setLayout(null);

        jLabel1.setFont(new java.awt.Font("Lao MN", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("House Environment");
        add(jLabel1);
        jLabel1.setBounds(16, 16, 159, 20);

        jLabel2.setText("Investigator Name:");
        add(jLabel2);
        jLabel2.setBounds(193, 16, 119, 16);

        namejLabel.setText("jLabel3");
        add(namejLabel);
        namejLabel.setBounds(377, 16, 45, 16);

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Choose community to view Environment:");
        add(jLabel3);
        jLabel3.setBounds(16, 53, 258, 16);

        communityjComboBox.setBackground(new java.awt.Color(255, 255, 255));
        communityjComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        communityjComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                communityjComboBoxActionPerformed(evt);
            }
        });
        add(communityjComboBox);
        communityjComboBox.setBounds(311, 49, 129, 27);

        environmentjTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Status", "Temperature", "Humidity", "Ozone Pollution", "Particle Pollution", "House Address", "User"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(environmentjTable);

        add(jScrollPane1);
        jScrollPane1.setBounds(16, 88, 800, 291);

        backjButton5.setBackground(new java.awt.Color(255, 255, 255));
        backjButton5.setText("<< Back");
        backjButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjButton5ActionPerformed(evt);
            }
        });
        add(backjButton5);
        backjButton5.setBounds(16, 455, 112, 29);

        jLabel5.setText("Filter:");
        add(jLabel5);
        jLabel5.setBounds(476, 53, 36, 16);

        filterDatejTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                filterDatejTextFieldKeyReleased(evt);
            }
        });
        add(filterDatejTextField);
        filterDatejTextField.setBounds(530, 48, 179, 26);

        sendjButton.setBackground(new java.awt.Color(255, 255, 255));
        sendjButton.setText("Send Detail to Environment Official");
        sendjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendjButtonActionPerformed(evt);
            }
        });
        add(sendjButton);
        sendjButton.setBounds(560, 400, 265, 29);

        checkjButton.setBackground(new java.awt.Color(255, 255, 255));
        checkjButton.setText("Check messages about House Environment");
        checkjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkjButtonActionPerformed(evt);
            }
        });
        add(checkjButton);
        checkjButton.setBounds(16, 397, 316, 29);

        backgroundjLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/background.jpg"))); // NOI18N
        add(backgroundjLabel);
        backgroundjLabel.setBounds(0, 0, 680, 540);
    }// </editor-fold>//GEN-END:initComponents

    private void communityjComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_communityjComboBoxActionPerformed
        // TODO add your handling code here:
        String selectedCommunity = String.valueOf(communityjComboBox.getSelectedItem());
        if (selectedCommunity == "Los Angeles") {

            popTableLA();
        } else if (selectedCommunity == "San Diego") {

            popTableSD();
        } else if (selectedCommunity == "Orange") {

            popTableO();
        }
    }//GEN-LAST:event_communityjComboBoxActionPerformed

    private void backjButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjButton5ActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backjButton5ActionPerformed

    private void filterDatejTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filterDatejTextFieldKeyReleased
        // TODO add your handling code here:
        String query = filterDatejTextField.getText();
        filter(query);

    }//GEN-LAST:event_filterDatejTextFieldKeyReleased

    private void sendjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendjButtonActionPerformed
        int selectedRow = environmentjTable.getSelectedRow();

        if (selectedRow >= 0) {
            date = String.valueOf(environmentjTable.getValueAt(selectedRow, 0));
            houseStatus = String.valueOf(environmentjTable.getValueAt(selectedRow, 1));
            temperature = String.valueOf(environmentjTable.getValueAt(selectedRow, 2));
            humidity = String.valueOf(environmentjTable.getValueAt(selectedRow, 3));
            ozone = String.valueOf(environmentjTable.getValueAt(selectedRow, 4));
            pm = String.valueOf(environmentjTable.getValueAt(selectedRow, 5));
            address = String.valueOf(environmentjTable.getValueAt(selectedRow, 6));
            username = String.valueOf(environmentjTable.getValueAt(selectedRow, 7));

            HouseMessageJPanel hmjp = new HouseMessageJPanel(userProcessContainer, userAccount, enterprise, date, houseStatus, temperature, humidity, ozone, pm, address, username);
            CardLayout cardlayout = (CardLayout) userProcessContainer.getLayout();
            userProcessContainer.add("HouseMessageJPanel", hmjp);
            cardlayout.next(userProcessContainer);
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Row");
        }

//System.out.println(username);

    }//GEN-LAST:event_sendjButtonActionPerformed

    private void checkjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkjButtonActionPerformed
        HouseMessageJPanel hmjp = new HouseMessageJPanel(userProcessContainer, userAccount, enterprise, null, null, null, null, null, null, null, null);
        CardLayout cardlayout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("HouseMessageJPanel", hmjp);
        cardlayout.next(userProcessContainer);
    }//GEN-LAST:event_checkjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel backgroundjLabel;
    private javax.swing.JButton backjButton5;
    private javax.swing.JButton checkjButton;
    private javax.swing.JComboBox communityjComboBox;
    private javax.swing.JTable environmentjTable;
    private javax.swing.JTextField filterDatejTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel namejLabel;
    private javax.swing.JButton sendjButton;
    // End of variables declaration//GEN-END:variables
}
