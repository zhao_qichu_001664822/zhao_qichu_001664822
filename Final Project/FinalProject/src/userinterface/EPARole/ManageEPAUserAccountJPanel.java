/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.EPARole;

import userinterface.HealthCareRole.*;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ZappyZhao
 */
public class ManageEPAUserAccountJPanel extends javax.swing.JPanel {

    private JPanel container;
    private Enterprise enterprise;
    /**
     * Creates new form ManageUserAccountJPanel
     */
    public ManageEPAUserAccountJPanel(JPanel container, Enterprise enterprise) {
        initComponents();
        this.enterprise = enterprise;
        this.container = container;
        backgroundjLabel.setSize(1000, 1000);
        popOrganizationComboBox();
       // employeeJComboBox.removeAllItems();
        popData();
    }

    public void popOrganizationComboBox() {
        organizationJComboBox.removeAllItems();

        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            organizationJComboBox.addItem(organization);
        }
    }
    
    public void populateEmployeeComboBox(Organization organization){
        employeeJComboBox.removeAllItems();
        
        for (Employee employee : organization.getEmployeeDirectory().getListOfEmployees()){
            employeeJComboBox.addItem(employee);
        }
    }
    
    private void populateRoleComboBox(Organization organization){
        roleJComboBox.removeAllItems();
        for (Role role : organization.getSupportedRole()){
            roleJComboBox.addItem(role);
        }
    }

    public void popData() {

        DefaultTableModel model = (DefaultTableModel) userJTable.getModel();

        model.setRowCount(0);

        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            for (UserAccount ua : organization.getUserAccountDirectory().getListOfUserAccounts()) {
                Object row[] = new Object[3];
                row[0] = ua;
                row[1] = ua.getRole();
                row[2] = organization;
                model.addRow(row);
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        userJTable = new javax.swing.JTable();
        organizationJComboBox = new javax.swing.JComboBox();
        employeeJComboBox = new javax.swing.JComboBox();
        roleJComboBox = new javax.swing.JComboBox();
        nameJTextField = new javax.swing.JTextField();
        passwordJTextField = new javax.swing.JTextField();
        backjButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        createUserJButton = new javax.swing.JButton();
        deletejButton1 = new javax.swing.JButton();
        backgroundjLabel = new javax.swing.JLabel();

        setLayout(null);

        userJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "User Name", "Role", "Organization"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(userJTable);

        add(jScrollPane1);
        jScrollPane1.setBounds(86, 41, 517, 179);

        organizationJComboBox.setBackground(new java.awt.Color(255, 255, 255));
        organizationJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        organizationJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                organizationJComboBoxActionPerformed(evt);
            }
        });
        add(organizationJComboBox);
        organizationJComboBox.setBounds(212, 261, 200, 27);

        employeeJComboBox.setBackground(new java.awt.Color(255, 255, 255));
        employeeJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        add(employeeJComboBox);
        employeeJComboBox.setBounds(212, 300, 200, 27);

        roleJComboBox.setBackground(new java.awt.Color(255, 255, 255));
        roleJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        add(roleJComboBox);
        roleJComboBox.setBounds(212, 339, 200, 27);
        add(nameJTextField);
        nameJTextField.setBounds(212, 372, 146, 26);
        add(passwordJTextField);
        passwordJTextField.setBounds(212, 416, 146, 26);

        backjButton1.setText("<< Back");
        backjButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjButton1ActionPerformed(evt);
            }
        });
        add(backjButton1);
        backjButton1.setBounds(86, 462, 112, 29);

        jLabel5.setText("Organization");
        add(jLabel5);
        jLabel5.setBounds(101, 265, 81, 16);

        jLabel3.setText("Employee");
        add(jLabel3);
        jLabel3.setBounds(101, 304, 60, 16);

        jLabel4.setText("Role");
        add(jLabel4);
        jLabel4.setBounds(101, 343, 27, 16);

        jLabel1.setText("User Name");
        add(jLabel1);
        jLabel1.setBounds(101, 377, 68, 16);

        jLabel2.setText("Password");
        add(jLabel2);
        jLabel2.setBounds(101, 421, 59, 16);

        createUserJButton.setBackground(new java.awt.Color(255, 255, 255));
        createUserJButton.setText("Create");
        createUserJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createUserJButtonActionPerformed(evt);
            }
        });
        add(createUserJButton);
        createUserJButton.setBounds(519, 465, 84, 29);

        deletejButton1.setBackground(new java.awt.Color(255, 255, 255));
        deletejButton1.setText("Delete");
        deletejButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletejButton1ActionPerformed(evt);
            }
        });
        add(deletejButton1);
        deletejButton1.setBounds(519, 226, 84, 29);

        backgroundjLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/background.jpg"))); // NOI18N
        add(backgroundjLabel);
        backgroundjLabel.setBounds(0, 0, 680, 540);
    }// </editor-fold>//GEN-END:initComponents

    private void organizationJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_organizationJComboBoxActionPerformed
        Organization organization = (Organization) organizationJComboBox.getSelectedItem();
        if (organization != null){
            populateEmployeeComboBox(organization);
            populateRoleComboBox(organization);
        }
    }//GEN-LAST:event_organizationJComboBoxActionPerformed

    private void backjButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjButton1ActionPerformed
        // TODO add your handling code here:
        container.remove(this);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.previous(container);
    }//GEN-LAST:event_backjButton1ActionPerformed

    private void createUserJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createUserJButtonActionPerformed
        Organization organization = (Organization) organizationJComboBox.getSelectedItem();
        Employee employee = (Employee) employeeJComboBox.getSelectedItem();
        Role role = (Role) roleJComboBox.getSelectedItem();
        String userName = nameJTextField.getText();
        String password = passwordJTextField.getText();

        boolean flag = false;

        if(nameJTextField.getText().trim().isEmpty() || passwordJTextField.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please complete the blanks", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        else if(organization == null || employee == null || role == null) {
            JOptionPane.showMessageDialog(this, "Invalid Input!");
            return;
        }
        else {
            for(UserAccount uaA : enterprise.getUserAccountDirectory().getListOfUserAccounts()) {
                if(uaA.getUsername().equals(userName)) {
                    JOptionPane.showMessageDialog(this, "This UserAccount Has Already Existed!");
                    flag = true;
                    break;
                }
            }
            for(UserAccount uaA : organization.getUserAccountDirectory().getListOfUserAccounts()) {
                if(uaA.getUsername().equals(userName)) {
                    JOptionPane.showMessageDialog(this, "This UserAccount Has Already Existed!");
                    flag = true;
                    break;
                }
            }
        }

        if(flag == false) {
            organization.getUserAccountDirectory().createUserAccount(userName, password, employee, role);
            JOptionPane.showMessageDialog(this, "User Account has been successfully added!", "Success", JOptionPane.PLAIN_MESSAGE);

        }
        nameJTextField.setText("");
        passwordJTextField.setText("");
        popData();

    }//GEN-LAST:event_createUserJButtonActionPerformed

    private void deletejButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletejButton1ActionPerformed
        int selectedRow = userJTable.getSelectedRow();
        if(selectedRow>=0){
            int warningDialog = JOptionPane.showConfirmDialog(this,"Would you like to delete this Account?","Warning",JOptionPane.YES_NO_OPTION);
            if(warningDialog == JOptionPane.YES_OPTION){
                UserAccount ua = (UserAccount)userJTable.getValueAt(selectedRow, 0);
                Organization organization = (Organization)userJTable.getValueAt(selectedRow, 2);
                organization.getUserAccountDirectory().deleteUserAccount(ua);

            }

        }
        else
        JOptionPane.showMessageDialog(this,"Please select a row","Warning",JOptionPane.PLAIN_MESSAGE);
        popData();
    }//GEN-LAST:event_deletejButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel backgroundjLabel;
    private javax.swing.JButton backjButton1;
    private javax.swing.JButton createUserJButton;
    private javax.swing.JButton deletejButton1;
    private javax.swing.JComboBox employeeJComboBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField nameJTextField;
    private javax.swing.JComboBox organizationJComboBox;
    private javax.swing.JTextField passwordJTextField;
    private javax.swing.JComboBox roleJComboBox;
    private javax.swing.JTable userJTable;
    // End of variables declaration//GEN-END:variables
}
