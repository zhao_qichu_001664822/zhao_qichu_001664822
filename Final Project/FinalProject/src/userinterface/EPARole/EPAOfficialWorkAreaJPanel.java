/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.EPARole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.HealthCareCenterEnterprise;
import Business.Network.Network;
import Business.Organization.EPAOfficeOrganization;
import Business.Organization.Organization;
import Business.Organization.UserOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.HouseMessageWorkRequest;
import Business.WorkQueue.IToEOWorkRequest;
import Business.WorkQueue.OfficialWorkRequest;
import Business.WorkQueue.WarnWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ZappyZhao
 */
public class EPAOfficialWorkAreaJPanel extends javax.swing.JPanel {
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private EPAOfficeOrganization ePAOfficeOrganization;
    private EcoSystem system;
    
    /**
     * Creates new form EPAOfficialWorkAreaJPanel
     */
    public EPAOfficialWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, Organization organization, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = account;
        this.ePAOfficeOrganization = (EPAOfficeOrganization)organization;
        this.system = system;
        backgroundjLabel.setSize(1000, 1000);
        namejLabel.setText(account.getEmployee().getName());
        
        populateTable();
        populateHCmessageTable();
        populateHouseTable();
    }
    
    public void populateTable(){
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        DefaultTableModel model = (DefaultTableModel)workRequestJTable.getModel();
        
        model.setRowCount(0);
        
        for(WorkRequest request : ePAOfficeOrganization.getWorkQueue().getListOfWorkRequests()){
            //System.out.println(request.getSender());
            if(request.getMessage() == null) {
                return;
            }
            else if(request.getMessage().startsWith("Date until:")) {
            
            Object[] row = new Object[7];
            row[0] = ((IToEOWorkRequest)request).getAbnormalPercentage();
            row[1] = request;
            row[2] = request.getSender().getEmployee().getName();
            row[3] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
            row[4] = request.getStatus();
            row[5] = dateFormat.format(request.getRequestDate());
            row[6] = request.getResolveDate() == null ? "" : dateFormat.format(request.getResolveDate());
            
            model.addRow(row);
                
            }
        }
    }
    
    public void populateHouseTable() {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        DefaultTableModel dtm = (DefaultTableModel)houseTable.getModel();
        dtm.setRowCount(0);
        for(WorkRequest request : ePAOfficeOrganization.getWorkQueue().getListOfWorkRequests()){
            //System.out.println(ePAOfficeOrganization.getWorkQueue().getListOfWorkRequests());
            if(request.getMessage() == null) {
                Object[] row = new Object[9];
                row[0] = ((HouseMessageWorkRequest)request).getHouseStatus();
                row[1] = ((HouseMessageWorkRequest)request);
                row[2] = request.getSender() == null ? null : request.getSender().getEmployee().getName();
                row[3] = request.getReceiver() == null ? "" : request.getReceiver().getEmployee().getName();
                row[4] = request.getStatus();
                row[5] = dateFormat.format(request.getRequestDate());
                row[6] = request.getResolveDate() == null ? "" : dateFormat.format(request.getResolveDate());
                row[7] = ((HouseMessageWorkRequest)request).getDate();
                row[8] = ((HouseMessageWorkRequest)request).getUsername();
                dtm.addRow(row);  
            }
        }
        
    }

    public void populateHCmessageTable() {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        DefaultTableModel dtm = (DefaultTableModel)HCmessageJTable.getModel();
        dtm.setRowCount(0);
           
        for(WorkRequest request : ePAOfficeOrganization.getWorkQueue().getListOfWorkRequests()) {
            //System.out.println(request.getMessage());
            
            if(request.getMessage()!=null && !request.getMessage().startsWith("Date until:")) {
                Object[] row = new Object[8];
                row[0] = ((OfficialWorkRequest)request).getAbnormalPercentage();
                row[1] = request;
                row[2] = request.getSender() == null ? null : request.getSender().getEmployee().getName();
                row[3] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
                row[4] = request.getStatus();
                row[5] = dateFormat.format(request.getRequestDate());
                row[6] = ((OfficialWorkRequest)request).getResult() == null ? "" : ((OfficialWorkRequest)request).getResult();
                row[7] = request.getResolveDate() == null ? "" : dateFormat.format(request.getResolveDate());
                dtm.addRow(row);
            }
        }
//        for(WorkRequest request : ePAOfficeOrganization.getWorkQueue().getListOfWorkRequests()) {
//            //System.out.println(request.getMessage());
//            if(request.getMessage()==null) {
//                return;
//            }
//            else if(request.getMessage().startsWith("Date until:")) {
//                return;
//            }
//            else {
            
//            }
//        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        namejLabel = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        HCmessageJTable = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        replyjButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        readJButton = new javax.swing.JButton();
        refreshJButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        houseTable = new javax.swing.JTable();
        assignJButton1 = new javax.swing.JButton();
        forwardToUserjButton = new javax.swing.JButton();
        backgroundjLabel = new javax.swing.JLabel();

        setLayout(null);

        jLabel1.setFont(new java.awt.Font("Lao MN", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("EPA Official Work Area");
        add(jLabel1);
        jLabel1.setBounds(15, 15, 180, 20);

        jLabel2.setFont(new java.awt.Font("Lao MN", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("EPA Official:");
        add(jLabel2);
        jLabel2.setBounds(203, 15, 87, 20);

        namejLabel.setFont(new java.awt.Font("Lao MN", 1, 13)); // NOI18N
        namejLabel.setText("jLabel3");
        add(namejLabel);
        namejLabel.setBounds(298, 15, 70, 19);

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Messages from Health Care Official (Vital Sign Data):");
        add(jLabel4);
        jLabel4.setBounds(15, 451, 330, 16);

        HCmessageJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Abnormal Percentage", "Message", "Sender", "Receiver", "Status", "Request Date", "Result", "Resolve Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(HCmessageJTable);

        add(jScrollPane2);
        jScrollPane2.setBounds(15, 479, 870, 141);

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Messages from Investigators (Community):");
        add(jLabel3);
        jLabel3.setBounds(15, 51, 270, 16);

        replyjButton.setBackground(new java.awt.Color(255, 255, 255));
        replyjButton.setText("Rely to Health Official");
        replyjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                replyjButtonActionPerformed(evt);
            }
        });
        add(replyjButton);
        replyjButton.setBounds(670, 630, 210, 29);

        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Abnormal Percentage", "Message", "Sender", "Receiver", "Status", "Request Date", "Resolve Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(workRequestJTable);

        add(jScrollPane1);
        jScrollPane1.setBounds(15, 79, 870, 116);

        readJButton.setBackground(new java.awt.Color(255, 255, 255));
        readJButton.setText("Mark as Read");
        readJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                readJButtonActionPerformed(evt);
            }
        });
        add(readJButton);
        readJButton.setBounds(750, 420, 127, 29);

        refreshJButton.setBackground(new java.awt.Color(255, 255, 255));
        refreshJButton.setText("Refresh");
        refreshJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshJButtonActionPerformed(evt);
            }
        });
        add(refreshJButton);
        refreshJButton.setBounds(780, 10, 91, 29);

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Messages from Investigators (House):");
        add(jLabel5);
        jLabel5.setBounds(15, 236, 237, 16);

        houseTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Environment Status", "Result", "Sender", "Receiver", "Message Status", "Request Date", "Resolve Date", "Data Date", "User Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(houseTable);

        add(jScrollPane3);
        jScrollPane3.setBounds(15, 264, 870, 146);

        assignJButton1.setBackground(new java.awt.Color(255, 255, 255));
        assignJButton1.setText("Mark as Read");
        assignJButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButton1ActionPerformed(evt);
            }
        });
        add(assignJButton1);
        assignJButton1.setBounds(750, 200, 127, 29);

        forwardToUserjButton.setBackground(new java.awt.Color(255, 255, 255));
        forwardToUserjButton.setText("Forward this to Community");
        forwardToUserjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forwardToUserjButtonActionPerformed(evt);
            }
        });
        add(forwardToUserjButton);
        forwardToUserjButton.setBounds(530, 200, 216, 29);

        backgroundjLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/background.jpg"))); // NOI18N
        add(backgroundjLabel);
        backgroundjLabel.setBounds(0, 0, 680, 540);
    }// </editor-fold>//GEN-END:initComponents

    private void replyjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_replyjButtonActionPerformed
        int selectedRow = HCmessageJTable.getSelectedRow();
        if(selectedRow>=0) {
            OfficialWorkRequest officialRequest = (OfficialWorkRequest)HCmessageJTable.getValueAt(selectedRow, 1);
            if(!officialRequest.getStatus().equals("Completed")) {
                officialRequest.setReceiver(userAccount);
                officialRequest.setStatus("Processing");
                
                WriteResultJPanel wrjp = new WriteResultJPanel(userProcessContainer, officialRequest);
                userProcessContainer.add("WriteResultJPanel", wrjp);
                CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            layout.next(userProcessContainer);
                
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a row!");
        }
        
        
        
    }//GEN-LAST:event_replyjButtonActionPerformed

    private void readJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_readJButtonActionPerformed
        int selectedRow = houseTable.getSelectedRow();

        if (selectedRow < 0){
            return;
        }

        HouseMessageWorkRequest request = (HouseMessageWorkRequest)houseTable.getValueAt(selectedRow, 1);
        request.setReceiver(userAccount);
        request.setStatus("Read");
        request.setResolveDate(new Date());
        populateHouseTable();
    }//GEN-LAST:event_readJButtonActionPerformed

    private void refreshJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshJButtonActionPerformed
        populateTable();
        populateHCmessageTable();
        populateHouseTable();
    }//GEN-LAST:event_refreshJButtonActionPerformed

    private void assignJButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButton1ActionPerformed
        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0){
            return;
        }

        IToEOWorkRequest request = (IToEOWorkRequest)workRequestJTable.getValueAt(selectedRow, 1);
        request.setReceiver(userAccount);
        request.setStatus("Read");
        request.setResolveDate(new Date());
        populateTable();
    }//GEN-LAST:event_assignJButton1ActionPerformed

    private void forwardToUserjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_forwardToUserjButtonActionPerformed
        String communityToWarn;
        int selectedRow = workRequestJTable.getSelectedRow();
        if(selectedRow>=0) {
            communityToWarn = String.valueOf(workRequestJTable.getValueAt(selectedRow, 1));
            if(communityToWarn.indexOf("Los Angeles")!=-1) {
                communityToWarn = "Los Angeles";
            } else if(communityToWarn.indexOf("San Diego")!=-1) {
                communityToWarn = "San Diego";
            } else if(communityToWarn.indexOf("Orange")!=-1) {
                communityToWarn = "Orange";
            } else {
                JOptionPane.showMessageDialog(this, "No Community Information");
            }
            
            WarnWorkRequest request = new WarnWorkRequest();
            request.setSender(userAccount);
            request.setRequestDate(new Date());
            request.setMessage("Environment Condition of "+communityToWarn+" is Abnormal these days. For those with breathing problems please stay at home.");
            request.setStatus("Sent");
            
            Organization org = null;
            for(Network network : system.getNetworkList()) {
                //System.out.println(network.getName());
                for(Enterprise enterpriseNew : network.getEnterpriseDirectory().getListOfEnterprises()) {
                    if(enterpriseNew instanceof HealthCareCenterEnterprise) {
                        for (Organization organization : enterpriseNew.getOrganizationDirectory().getOrganizationList()) {
                            if (organization instanceof UserOrganization) {
                                org = organization;
                                break;
                            }
                        }
                    }
                }
            }
            
            if(org != null) {
                org.getWorkQueue().getListOfWorkRequests().add(request);
                //userAccount.getWorkQueue().getListOfWorkRequests().add(request);
                JOptionPane.showMessageDialog(this, "Your message has been sent!");
            }
            
//System.out.println(request.getMessage());
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Row");
        }
        populateTable();
    }//GEN-LAST:event_forwardToUserjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable HCmessageJTable;
    private javax.swing.JButton assignJButton1;
    private javax.swing.JLabel backgroundjLabel;
    private javax.swing.JButton forwardToUserjButton;
    private javax.swing.JTable houseTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel namejLabel;
    private javax.swing.JButton readJButton;
    private javax.swing.JButton refreshJButton;
    private javax.swing.JButton replyjButton;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables
}
