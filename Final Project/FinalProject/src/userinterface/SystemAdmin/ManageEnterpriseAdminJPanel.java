/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.SystemAdmin;

import Business.EcoSystem;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Role.EPAAdminRole;
import Business.Role.GovernmentAdminRole;
import Business.Role.HealthAdminRole;
import Business.Role.MayorRole;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ZappyZhao
 */
public class ManageEnterpriseAdminJPanel extends javax.swing.JPanel {
    
    private JPanel userProcessContainer;
    private EcoSystem system;
    
    /**
     * Creates new form ManageEnterpriseAdminJPanel
     */
    public ManageEnterpriseAdminJPanel(JPanel userProcessContainer, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.system = system;
        backgroundjLabel.setSize(1000, 1000);

        populateTable();
        populateNetworkComboBox();
    }

    private void populateTable() {
        DefaultTableModel model = (DefaultTableModel) enterpriseJTable.getModel();

        model.setRowCount(0);
        for (Network network : system.getNetworkList()) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getListOfEnterprises()) {
                for (UserAccount userAccount : enterprise.getUserAccountDirectory().getListOfUserAccounts()) {
                    Object[] row = new Object[3];
                    row[0] = enterprise;
                    row[1] = network;
                    row[2] = userAccount;

                    model.addRow(row);
                }
            }
        }
    }
    
    private void populateNetworkComboBox(){
        networkJComboBox.removeAllItems();
        
        for (Network network : system.getNetworkList()){
            networkJComboBox.addItem(network);
        }
    }
    
    private void populateEnterpriseComboBox(Network network){
        enterpriseJComboBox.removeAllItems();
        
        for (Enterprise enterprise : network.getEnterpriseDirectory().getListOfEnterprises()){
            enterpriseJComboBox.addItem(enterprise);
        }
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        enterpriseJTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        networkJComboBox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        enterpriseJComboBox = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        usernameJTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        passwordJPasswordField = new javax.swing.JPasswordField();
        jLabel5 = new javax.swing.JLabel();
        nameJTextField = new javax.swing.JTextField();
        backJButton = new javax.swing.JButton();
        submitJButton = new javax.swing.JButton();
        deletejButton = new javax.swing.JButton();
        backgroundjLabel = new javax.swing.JLabel();

        setLayout(null);

        enterpriseJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Enterprise Name", "Network", "Username"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(enterpriseJTable);

        add(jScrollPane1);
        jScrollPane1.setBounds(39, 58, 523, 95);

        jLabel1.setFont(new java.awt.Font("Lao MN", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Network");
        add(jLabel1);
        jLabel1.setBounds(88, 211, 80, 20);

        networkJComboBox.setBackground(new java.awt.Color(255, 255, 255));
        networkJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        networkJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                networkJComboBoxActionPerformed(evt);
            }
        });
        add(networkJComboBox);
        networkJComboBox.setBounds(203, 207, 136, 27);

        jLabel3.setFont(new java.awt.Font("Lao MN", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Enterprise");
        add(jLabel3);
        jLabel3.setBounds(88, 269, 100, 20);

        enterpriseJComboBox.setBackground(new java.awt.Color(255, 255, 255));
        enterpriseJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        add(enterpriseJComboBox);
        enterpriseJComboBox.setBounds(203, 265, 136, 27);

        jLabel2.setFont(new java.awt.Font("Lao MN", 1, 13)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Username");
        add(jLabel2);
        jLabel2.setBounds(89, 315, 80, 20);
        add(usernameJTextField);
        usernameJTextField.setBounds(203, 310, 136, 26);

        jLabel4.setFont(new java.awt.Font("Lao MN", 1, 13)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Password");
        add(jLabel4);
        jLabel4.setBounds(89, 359, 80, 19);
        add(passwordJPasswordField);
        passwordJPasswordField.setBounds(202, 354, 134, 26);

        jLabel5.setFont(new java.awt.Font("Lao MN", 1, 13)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Name");
        add(jLabel5);
        jLabel5.setBounds(89, 403, 70, 20);
        add(nameJTextField);
        nameJTextField.setBounds(200, 400, 136, 26);

        backJButton.setBackground(new java.awt.Color(255, 255, 255));
        backJButton.setFont(new java.awt.Font("Lao MN", 1, 13)); // NOI18N
        backJButton.setText("<< Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });
        add(backJButton);
        backJButton.setBounds(40, 440, 96, 29);

        submitJButton.setBackground(new java.awt.Color(255, 255, 255));
        submitJButton.setFont(new java.awt.Font("Lao MN", 1, 13)); // NOI18N
        submitJButton.setText("Submit");
        submitJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitJButtonActionPerformed(evt);
            }
        });
        add(submitJButton);
        submitJButton.setBounds(470, 440, 89, 29);

        deletejButton.setBackground(new java.awt.Color(255, 255, 255));
        deletejButton.setFont(new java.awt.Font("Lao MN", 1, 13)); // NOI18N
        deletejButton.setText("Delete");
        deletejButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletejButtonActionPerformed(evt);
            }
        });
        add(deletejButton);
        deletejButton.setBounds(478, 171, 88, 29);

        backgroundjLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/background.jpg"))); // NOI18N
        add(backgroundjLabel);
        backgroundjLabel.setBounds(0, 0, 680, 480);
    }// </editor-fold>//GEN-END:initComponents

    private void networkJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_networkJComboBoxActionPerformed

        Network network = (Network) networkJComboBox.getSelectedItem();
        if (network != null){
            populateEnterpriseComboBox(network);
        }

    }//GEN-LAST:event_networkJComboBoxActionPerformed

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        SystemAdminWorkAreaJPanel sysAdminwjp = (SystemAdminWorkAreaJPanel) component;
        sysAdminwjp.populateTree();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed

    private void submitJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitJButtonActionPerformed
        Network network = (Network) networkJComboBox.getSelectedItem();
        Enterprise enterprise = (Enterprise) enterpriseJComboBox.getSelectedItem();

        String username = usernameJTextField.getText();
        String password = String.valueOf(passwordJPasswordField.getPassword());
        String name = nameJTextField.getText();
        
        boolean flag = false;
        
        if(usernameJTextField.getText().trim().isEmpty() || nameJTextField.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please complete all blanks");
            return;
        }
        else if(network == null || enterprise == null) {
            JOptionPane.showMessageDialog(this, "Invalid Input!");
            return;
        }
        else {
            for(UserAccount userAccountA : enterprise.getUserAccountDirectory().getListOfUserAccounts()) {
                if(userAccountA.getUsername().equals(username)) {
                    //System.out.println(userAccountA.getUsername());
                    JOptionPane.showMessageDialog(this, "This UserAccount Has Already Existed!");
                    flag = true;
                    break;
                }
            }
        }
        if(flag == false) {
            if (enterprise.getEnterpriseType() == Enterprise.EnterpriseType.HealthCareCenter) {
                Employee employee = enterprise.getEmployeeDirectory().createEmployee(name);
                enterprise.getUserAccountDirectory().createUserAccount(username, password, employee, new HealthAdminRole());
            } else if (enterprise.getEnterpriseType() == Enterprise.EnterpriseType.Government) {
                Employee employee = enterprise.getEmployeeDirectory().createEmployee(name);
                enterprise.getUserAccountDirectory().createUserAccount(username, password, employee, new GovernmentAdminRole());
            } else if (enterprise.getEnterpriseType() == Enterprise.EnterpriseType.EPA) {
                Employee employee = enterprise.getEmployeeDirectory().createEmployee(name);
                enterprise.getUserAccountDirectory().createUserAccount(username, password, employee, new EPAAdminRole());
            }
            JOptionPane.showMessageDialog(this, "New User Account Successfully Added!");
        }
        populateTable();
        
        usernameJTextField.setText("");
        passwordJPasswordField.setText("");
        nameJTextField.setText("");
    }//GEN-LAST:event_submitJButtonActionPerformed

    private void deletejButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletejButtonActionPerformed
        int selectedRow = enterpriseJTable.getSelectedRow();
        if(selectedRow>=0){
            int warningDialog = JOptionPane.showConfirmDialog(this,"Would you like to delete this Account?","Warning",JOptionPane.YES_NO_OPTION);
            if(warningDialog == JOptionPane.YES_OPTION){
                UserAccount u = (UserAccount)enterpriseJTable.getValueAt(selectedRow, 2);
                Enterprise enterprise = (Enterprise)enterpriseJTable.getValueAt(selectedRow, 0);
                enterprise.getUserAccountDirectory().deleteUserAccount(u);
            }
        }
        else
        JOptionPane.showMessageDialog(this,"Please select a row","Warning",JOptionPane.PLAIN_MESSAGE);
        populateTable();
    }//GEN-LAST:event_deletejButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backJButton;
    private javax.swing.JLabel backgroundjLabel;
    private javax.swing.JButton deletejButton;
    private javax.swing.JComboBox enterpriseJComboBox;
    private javax.swing.JTable enterpriseJTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField nameJTextField;
    private javax.swing.JComboBox networkJComboBox;
    private javax.swing.JPasswordField passwordJPasswordField;
    private javax.swing.JButton submitJButton;
    private javax.swing.JTextField usernameJTextField;
    // End of variables declaration//GEN-END:variables
}
