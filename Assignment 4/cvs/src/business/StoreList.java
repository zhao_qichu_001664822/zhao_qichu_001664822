/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class StoreList {
    private ArrayList<Store> listOfStores;
    
    public StoreList() {
        listOfStores = new ArrayList<Store>();
    }

    public ArrayList<Store> getListOfStores() {
        return listOfStores;
    }

    public void setListOfStores(ArrayList<Store> listOfStores) {
        this.listOfStores = listOfStores;
    }
    
    public Store addStore() {
        Store store = new Store();
        listOfStores.add(store);
        return store;
    }
    
    public void deleteStore(Store store) {
        listOfStores.remove(store);
    }
}
