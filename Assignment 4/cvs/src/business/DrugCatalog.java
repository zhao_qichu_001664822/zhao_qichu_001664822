/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class DrugCatalog {
    private ArrayList<Drug> listOfDrugs;

    public DrugCatalog() {
        listOfDrugs = new ArrayList<Drug>();
    }

    public ArrayList<Drug> getListOfDrugs() {
        return listOfDrugs;
    }

    public void setListOfDrugs(ArrayList<Drug> listOfDrugs) {
        this.listOfDrugs = listOfDrugs;
    }

    
    public Drug addDrug() {
        Drug d = new Drug();
        listOfDrugs.add(d);
        return d;
    }
    
    public void deleteDrug(Drug drug) {
        listOfDrugs.remove(drug);
    }
    
}
