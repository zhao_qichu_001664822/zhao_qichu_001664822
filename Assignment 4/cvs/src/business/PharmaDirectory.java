/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class PharmaDirectory {
    private ArrayList<Pharma> listOfPharmas;

    public PharmaDirectory() {
        listOfPharmas = new ArrayList<Pharma>();
    }

    public ArrayList<Pharma> getListOfPharmas() {
        return listOfPharmas;
    }

    public void setListOfPharmas(ArrayList<Pharma> listOfPharmas) {
        this.listOfPharmas = listOfPharmas;
    }
    
    public Pharma addPharma(){
        Pharma p = new Pharma();
        listOfPharmas.add(p);
        return p;
    }
    
}
