/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author ZappyZhao
 */
public class Pharma {
    private String pharmaName;
    private String pharmaID;
    private String phamaLocation;
    private DrugCatalog drugCatalog;

    public Pharma() {
        drugCatalog = new DrugCatalog();
    }

    public String getPharmaName() {
        return pharmaName;
    }

    public void setPharmaName(String pharmaName) {
        this.pharmaName = pharmaName;
    }

    public String getPharmaID() {
        return pharmaID;
    }

    public void setPharmaID(String pharmaID) {
        this.pharmaID = pharmaID;
    }
    
    public String getPhamaLocation() {
        return phamaLocation;
    }

    public void setPhamaLocation(String phamaLocation) {
        this.phamaLocation = phamaLocation;
    }

    public DrugCatalog getDrugCatalog() {
        return drugCatalog;
    }

    public void setDrugCatalog(DrugCatalog drugCatalog) {
        this.drugCatalog = drugCatalog;
    }

    @Override
    public String toString() {
        return pharmaID;
    }
    
    
}
