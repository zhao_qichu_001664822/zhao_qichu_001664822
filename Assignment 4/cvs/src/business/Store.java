/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;

/**
 *
 * @author ZappyZhao
 */
public class Store {
    private String address;
    private String storeNumber;
    private String storeTel;
    private int zipCode;
    private DrugInventory drugInventory;
    
    public Store() {
        drugInventory = new DrugInventory();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public String getStoreTel() {
        return storeTel;
    }

    public void setStoreTel(String storeTel) {
        this.storeTel = storeTel;
    }    

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public DrugInventory getDrugInventory() {
        return drugInventory;
    }

    public void setDrugInventory(DrugInventory drugInventory) {
        this.drugInventory = drugInventory;
    }

    @Override
    public String toString() {
        return storeNumber;
    }
    
    
}
