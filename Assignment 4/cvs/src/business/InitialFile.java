/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author ZappyZhao
 */
public class InitialFile {
    private StoreList storeList;
    private PharmaDirectory pharmaDirectory;
    private DrugCatalog drugCatalog;

    public InitialFile(StoreList storeList, PharmaDirectory pharmaDirectory, DrugCatalog drugCatalog) {
        this.storeList = storeList;
        this.pharmaDirectory = pharmaDirectory;
        this.drugCatalog = drugCatalog;
        //Store A:
        Store storeA = storeList.addStore();
        storeA.setAddress("aaa");
        storeA.setStoreNumber("Aa1");
        storeA.setStoreTel("1(123)1231231");
        storeA.setZipCode(11001);
        //Store B:
        Store storeB = storeList.addStore();
        storeB.setAddress("bbb");
        storeB.setStoreNumber("Bb2");
        storeB.setStoreTel("1(456)4564564");
        storeB.setZipCode(22002);
        //Pharma PharmaA:
        Pharma pharmaA = pharmaDirectory.addPharma();
        pharmaA.setPharmaName("Pharma A");
        pharmaA.setPharmaID("PA111");
        pharmaA.setPhamaLocation("New York");
        
//DrugAfirst:
        Drug drugAfirst = drugCatalog.addDrug();
        
        drugAfirst.setDrugName("Drug A First");
        drugAfirst.setProductionDate("2015.1.1");
        drugAfirst.setIngredient("a,b,c");
        drugAfirst.setRemedy("a,b,c");
        drugAfirst.setPrinciple("a,b,c");
        drugAfirst.setAllergic("a,b,c");
        drugAfirst.setAimCrowd("a,b,c");
        drugAfirst.setStorage("a,b,c");
        drugAfirst.setSideEffect("a,b,c");
        
        drugAfirst.setPrice(10);
        drugAfirst.setPharmaName("pharmaA");
        
        
//DrugAsecond:
        Drug drugAsecond = drugCatalog.addDrug();
        
        drugAsecond.setDrugName("Drug A Second");
        
        drugAsecond.setProductionDate("2015.2.2");
        drugAsecond.setIngredient("a,b,c");
        drugAsecond.setRemedy("a,b,c");
        drugAsecond.setPrinciple("a,b,c");
        drugAsecond.setAllergic("a,b,c");
        drugAsecond.setAimCrowd("a,b,c");
        drugAsecond.setStorage("a,b,c");
        drugAsecond.setSideEffect("a,b,c");
        
        drugAsecond.setPrice(20);
        drugAsecond.setPharmaName("pharmaA");
        
        pharmaA.setDrugCatalog(drugCatalog);
        //System.out.println(pharmaDirectory);
        
        //Pharma PharmaB:
        Pharma pharmaB = pharmaDirectory.addPharma();
        pharmaB.setPharmaName("Pharma B");
        pharmaB.setPharmaID("PB222");
        pharmaB.setPhamaLocation("Boston");
        
//DrugBfirst:
        Drug drugBfirst = drugCatalog.addDrug();
        drugBfirst.setDrugName("Drug B First");
        drugBfirst.setProductionDate("2015.3.3");
        drugBfirst.setIngredient("x,y,z");
        drugBfirst.setRemedy("x,y,z");
        drugBfirst.setPrinciple("x,y,z");
        drugBfirst.setAllergic("x,y,z");
        drugBfirst.setAimCrowd("x,y,z");
        drugBfirst.setStorage("x,y,z");
        drugBfirst.setSideEffect("x,y,z");
        
        drugBfirst.setPrice(30);
        drugBfirst.setPharmaName("pharmaB");
        
//DrugBsecond:
        Drug drugBsecond = drugCatalog.addDrug();
        drugBsecond.setDrugName("Drug B Second");
        drugBsecond.setProductionDate("2015.4.4");
        drugBsecond.setIngredient("x,y,z");
        drugBsecond.setRemedy("x,y,z");
        drugBsecond.setPrinciple("x,y,z");
        drugBsecond.setAllergic("x,y,z");
        drugBsecond.setAimCrowd("x,y,z");
        drugBsecond.setStorage("x,y,z");
        drugBsecond.setSideEffect("x,y,z");
        
        drugBsecond.setPrice(40);
        drugBsecond.setPharmaName("pharmaB");
        
        pharmaB.setDrugCatalog(drugCatalog);
        
    }
    
    
}
