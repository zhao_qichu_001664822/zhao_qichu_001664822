/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class DrugInventory {
    private ArrayList<Drug> listOfInventoryDrugs;
    

    public DrugInventory() {
        listOfInventoryDrugs = new ArrayList<Drug>();
    }

    public ArrayList<Drug> getListOfInventoryDrugs() {
        return listOfInventoryDrugs;
    }

    public void setListOfInventoryDrugs(ArrayList<Drug> listOfInventoryDrugs) {
        this.listOfInventoryDrugs = listOfInventoryDrugs;
    }

    public Drug addDrugItem() {
        Drug d = new Drug();
        listOfInventoryDrugs.add(d);
        return d;
    }
    
    public void deleteDrug(Drug drug) {
        listOfInventoryDrugs.remove(drug);
    }
}
