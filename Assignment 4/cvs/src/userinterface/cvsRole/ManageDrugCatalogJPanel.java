/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.cvsRole;

import business.Drug;
import business.DrugCatalog;
import business.InitialFile;
import business.Pharma;
import business.PharmaDirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ZappyZhao
 */
public class ManageDrugCatalogJPanel extends javax.swing.JPanel {
    private JPanel containerJPanel;
    private PharmaDirectory pharmaDirectory;
    private DrugCatalog drugCatalog;
    private InitialFile initialFile;
    /**
     * Creates new form ManageDrugCatalogJPanel
     */
    public ManageDrugCatalogJPanel(JPanel containerJPanel, PharmaDirectory pharmaDirectory, DrugCatalog drugCatalog, InitialFile initialFile) {
        initComponents();
        this.containerJPanel = containerJPanel;
        this.pharmaDirectory = pharmaDirectory;
        this.drugCatalog = drugCatalog;
        this.initialFile = initialFile;
        populateDrugCatalogjTable();
    }

    public void populateDrugCatalogjTable() {
        DefaultTableModel dtm = (DefaultTableModel)drugCatalogjTable.getModel();
        dtm.setRowCount(0);
        
        for(Drug drug : drugCatalog.getListOfDrugs()) {
                Object row[] = new Object[7];
                row[0] = drug;
                row[1] = drug.getPharmaName();
                row[2] = drug.getPrice();
                row[3] = drug.getProductionDate();
                row[4] = drug.getAllergic();
                row[5] = drug.getStorage();
                row[6] = drug.getSideEffect();
                
                dtm.addRow(row);
        }
       
        
        /*
        for(Pharma pharma : pharmaDirectory.getListOfPharmas()) {
 
            for(Drug drug : pharma.getDrugCatalog().getListOfDrugs()) {
               // pharma.getDrugCatalog().getListOfDrugs().get(0).getDrugName();
                Object row[] = new Object[3];
                row[0] = drug.getDrugName();
                row[1] = pharma.getPharmaName();
                row[2] = drug.getPrice();
                dtm.addRow(row);
            }
        }
        */
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        backjButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        drugCatalogjTable = new javax.swing.JTable();
        addPharmajButton = new javax.swing.JButton();
        addDrugjButton = new javax.swing.JButton();
        deleteDrugjButton = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Arial Black", 1, 14)); // NOI18N
        jLabel1.setText("Manage Drug Catalog");

        backjButton.setText("<<Back");
        backjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjButtonActionPerformed(evt);
            }
        });

        drugCatalogjTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Drug", "Pharma", "Price", "Product Date", "Allergic", "Storage", "Side Effect"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(drugCatalogjTable);

        addPharmajButton.setText("ADD Pharma");
        addPharmajButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addPharmajButtonActionPerformed(evt);
            }
        });

        addDrugjButton.setText("Add Drug");
        addDrugjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addDrugjButtonActionPerformed(evt);
            }
        });

        deleteDrugjButton.setText("Delete Drug");
        deleteDrugjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteDrugjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 689, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(addPharmajButton)
                                .addGap(40, 40, 40)
                                .addComponent(addDrugjButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(deleteDrugjButton))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(backjButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(jLabel1)))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addPharmajButton)
                    .addComponent(addDrugjButton)
                    .addComponent(deleteDrugjButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(backjButton)
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjButtonActionPerformed
        containerJPanel.remove(this);
        CardLayout layout = (CardLayout)containerJPanel.getLayout();
        layout.previous(containerJPanel);
    }//GEN-LAST:event_backjButtonActionPerformed

    private void addPharmajButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addPharmajButtonActionPerformed
        ManagePharmaJPanel mpjp = new ManagePharmaJPanel(containerJPanel, pharmaDirectory, this);
        containerJPanel.add("ManagePharma", mpjp);
        CardLayout cardLayout = (CardLayout)containerJPanel.getLayout();
        cardLayout.next(containerJPanel);   
    }//GEN-LAST:event_addPharmajButtonActionPerformed

    private void addDrugjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addDrugjButtonActionPerformed
        ChoosePharmaJPanel cpjp = new ChoosePharmaJPanel(containerJPanel, pharmaDirectory, drugCatalog, this);
        containerJPanel.add("ChoosePharma", cpjp);
        CardLayout cardLayout = (CardLayout)containerJPanel.getLayout();
        cardLayout.next(containerJPanel); 
    }//GEN-LAST:event_addDrugjButtonActionPerformed

    private void deleteDrugjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteDrugjButtonActionPerformed
        int selectedRow = drugCatalogjTable.getSelectedRow();
        if(selectedRow >=0) {
            int warningDialog = JOptionPane.showConfirmDialog(this,"Would you like to delete this drug?","Warning",JOptionPane.YES_NO_OPTION);
            if(warningDialog == JOptionPane.YES_OPTION){
                Drug drug = (Drug)drugCatalogjTable.getValueAt(selectedRow, 0);
                drugCatalog.deleteDrug(drug);
                populateDrugCatalogjTable();
            }
        }
        else
            JOptionPane.showMessageDialog(this,"Please select a row","Warning",JOptionPane.PLAIN_MESSAGE);    
    }//GEN-LAST:event_deleteDrugjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addDrugjButton;
    private javax.swing.JButton addPharmajButton;
    private javax.swing.JButton backjButton;
    private javax.swing.JButton deleteDrugjButton;
    private javax.swing.JTable drugCatalogjTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
