/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

public class Suppliers {
    private String supplierName;
    private ProductCatalog productCatalog;
    private Product product;
    private ArrayList<Product> dellProductList;
    private ArrayList<Product> hpProductList;
    private ArrayList<Product> toshibaProductList;
    private ArrayList<Product> appleProductList;
    private ArrayList<Product> lenovoProductList;
    
    public Suppliers(){
        productCatalog = new ProductCatalog();
        product = new Product();
        dellProductList = new ArrayList<>();
        hpProductList = new ArrayList<>();
        toshibaProductList = new ArrayList<>();
        appleProductList = new ArrayList<>();
        lenovoProductList = new ArrayList<>();
        
    }
    

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
// Add new Product
    
    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }


    public ArrayList<Product> getDellProductList() {
        return dellProductList;
    }

    public void setDellProductList(ArrayList<Product> dellProductList) {
        this.dellProductList = dellProductList;
    }

    public ArrayList<Product> getHpProductList() {
        return hpProductList;
    }

    public void setHpProductList(ArrayList<Product> hpProductList) {
        this.hpProductList = hpProductList;
    }

    public ArrayList<Product> getToshibaProductList() {
        return toshibaProductList;
    }

    public void setToshibaProductList(ArrayList<Product> toshibaProductList) {
        this.toshibaProductList = toshibaProductList;
    }

    public ArrayList<Product> getAppleProductList() {
        return appleProductList;
    }

    public void setAppleProductList(ArrayList<Product> appleProductList) {
        this.appleProductList = appleProductList;
    }

    public ArrayList<Product> getLenovoProductList() {
        return lenovoProductList;
    }

    public void setLenovoProductList(ArrayList<Product> lenovoProductList) {
        this.lenovoProductList = lenovoProductList;
    }
    
    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        if(supplierName == "Dell"){
            return supplierName + " :" + productCatalog.getDellProductList().toString() + "\n" ;
        }
        else if(supplierName == "HP"){
            return supplierName + " :" + productCatalog.getHpProductList().toString() + "\n" ; 
        }
        else if(supplierName == "Toshiba"){
            return supplierName + " :" + productCatalog.getToshibaProductList().toString() + "\n" ; 
        }
        else if(supplierName == "Apple"){
            return supplierName + " :" + productCatalog.getAppleProductList().toString() + "\n" ; 
        }
        else if(supplierName == "Lenovo"){
            return supplierName + " :" + productCatalog.getLenovoProductList().toString() + "\n" ; 
        }
        return null;
    }
    
    
}
