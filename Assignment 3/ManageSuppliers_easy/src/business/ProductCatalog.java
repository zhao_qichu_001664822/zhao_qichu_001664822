/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */

public class ProductCatalog {
    private ArrayList<Product> dellProductList;
    private ArrayList<Product> hpProductList;
    private ArrayList<Product> toshibaProductList;
    private ArrayList<Product> appleProductList;
    private ArrayList<Product> lenovoProductList;
    
    public ProductCatalog(){
        dellProductList = new ArrayList<>();
        hpProductList = new ArrayList<>();
        toshibaProductList = new ArrayList<>();
        appleProductList = new ArrayList<>();
        lenovoProductList = new ArrayList<>();
    }

    public ArrayList<Product> getDellProductList() {
        return dellProductList;
    }

    public void setDellProductList(ArrayList<Product> dellProductList) {
        this.dellProductList = dellProductList;
    }

    public ArrayList<Product> getHpProductList() {
        return hpProductList;
    }

    public void setHpProductList(ArrayList<Product> hpProductList) {
        this.hpProductList = hpProductList;
    }

    public ArrayList<Product> getToshibaProductList() {
        return toshibaProductList;
    }

    public void setToshibaProductList(ArrayList<Product> toshibaProductList) {
        this.toshibaProductList = toshibaProductList;
    }

    public ArrayList<Product> getAppleProductList() {
        return appleProductList;
    }

    public void setAppleProductList(ArrayList<Product> appleProductList) {
        this.appleProductList = appleProductList;
    }

    public ArrayList<Product> getLenovoProductList() {
        return lenovoProductList;
    }

    public void setLenovoProductList(ArrayList<Product> lenovoProductList) {
        this.lenovoProductList = lenovoProductList;
    }
    
}