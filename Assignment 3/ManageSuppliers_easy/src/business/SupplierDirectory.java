/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

public class SupplierDirectory {
    private ArrayList<Suppliers> listOfSuppliers;

    public SupplierDirectory() {
        listOfSuppliers = new ArrayList<Suppliers>();
    }

    public ArrayList<Suppliers> getListOfSuppliers() {
        return listOfSuppliers;
    }

    public void setListOfSuppliers(ArrayList<Suppliers> listOfSuppliers) {
        this.listOfSuppliers = listOfSuppliers;
    }

    public Suppliers addSupplier(){
        Suppliers splrs = new Suppliers();
        listOfSuppliers.add(splrs);
        return splrs;
    }
    
}
