/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

public class Main {
    private SupplierDirectory supplierDirectory;
    private ArrayList<Suppliers> suppliersList;
    
    private ProductCatalog productCatalog;
    private ArrayList<Product> dellProductList;
    private ArrayList<Product> hpProductList;
    private ArrayList<Product> toshibaProductList;
    private ArrayList<Product> appleProductList;
    private ArrayList<Product> lenovoProductList;
//    private Suppliers suppliers;
            
    public Main(){
        supplierDirectory = new SupplierDirectory();
        suppliersList = new ArrayList<Suppliers>();
        
        productCatalog = new ProductCatalog();
         dellProductList = new ArrayList<>();
        hpProductList = new ArrayList<>();
        toshibaProductList = new ArrayList<>();
        appleProductList = new ArrayList<>();
        lenovoProductList = new ArrayList<>();
        
//        suppliers = new Suppliers();
        
// Add Suppliers Information
        Suppliers dell = new Suppliers();
        dell.setSupplierName("Dell");
        Suppliers hp = new Suppliers();
        hp.setSupplierName("HP");
        Suppliers toshiba = new Suppliers();
        toshiba.setSupplierName("Toshiba");
        Suppliers apple = new Suppliers();
        apple.setSupplierName("Apple");
        Suppliers lenovo = new Suppliers();
        lenovo.setSupplierName("Lenovo");
        
//Add Products Information
        
    //Dell
        Product dellProduct1 = new Product();
        dellProduct1.setProductName("D1");
        dellProduct1.setProductInfo("D1 Info");
        Product dellProduct2 = new Product();
        dellProduct2.setProductName("D2");
        dellProduct2.setProductInfo("D2 Info");
        Product dellProduct3 = new Product();
        dellProduct3.setProductName("D3");
        dellProduct3.setProductInfo("D3 Info");
        Product dellProduct4 = new Product();
        dellProduct4.setProductName("D4");
        dellProduct4.setProductInfo("D4 Info");
        Product dellProduct5 = new Product();
        dellProduct5.setProductName("D5");
        dellProduct5.setProductInfo("D5 Info");
        Product dellProduct6 = new Product();
        dellProduct6.setProductName("D6");
        dellProduct6.setProductInfo("D6 Info");
        Product dellProduct7 = new Product();
        dellProduct7.setProductName("D7");
        dellProduct7.setProductInfo("D7 Info");
        Product dellProduct8 = new Product();
        dellProduct8.setProductName("D8");
        dellProduct8.setProductInfo("D8 Info");
        Product dellProduct9 = new Product();
        dellProduct9.setProductName("D9");
        dellProduct9.setProductInfo("D9 Info");
        Product dellProduct10 = new Product();
        dellProduct10.setProductName("D10");
        dellProduct10.setProductInfo("D10 Info");
        
        dellProductList.add(dellProduct1);
        dellProductList.add(dellProduct2);
        dellProductList.add(dellProduct3);
        dellProductList.add(dellProduct4);
        dellProductList.add(dellProduct5);
        dellProductList.add(dellProduct6);
        dellProductList.add(dellProduct7);
        dellProductList.add(dellProduct8);
        dellProductList.add(dellProduct9);
        dellProductList.add(dellProduct10);
        productCatalog.setDellProductList(dellProductList);
        
    //HP
        Product hpProduct1 = new Product();
        hpProduct1.setProductName("H1");
        hpProduct1.setProductInfo("H1 Info");
        Product hpProduct2 = new Product();
        hpProduct2.setProductName("H2");
        hpProduct2.setProductInfo("H2 Info");
        Product hpProduct3 = new Product();
        hpProduct3.setProductName("H3");
        hpProduct3.setProductInfo("H3 Info");
        Product hpProduct4 = new Product();
        hpProduct4.setProductName("H4");
        hpProduct4.setProductInfo("H4 Info");
        Product hpProduct5 = new Product();
        hpProduct5.setProductName("H5");
        hpProduct5.setProductInfo("H5 Info");
        Product hpProduct6 = new Product();
        hpProduct6.setProductName("H6");
        hpProduct6.setProductInfo("H6 Info");
        Product hpProduct7 = new Product();
        hpProduct7.setProductName("H7");
        hpProduct7.setProductInfo("H7 Info");
        Product hpProduct8 = new Product();
        hpProduct8.setProductName("H8");
        hpProduct8.setProductInfo("H8 Info");
        Product hpProduct9 = new Product();
        hpProduct9.setProductName("H9");
        hpProduct9.setProductInfo("H9 Info");
        Product hpProduct10 = new Product();
        hpProduct10.setProductName("H10");
        hpProduct10.setProductInfo("H10 Info");
        
        hpProductList.add(hpProduct1);
        hpProductList.add(hpProduct2);
        hpProductList.add(hpProduct3);
        hpProductList.add(hpProduct4);
        hpProductList.add(hpProduct5);
        hpProductList.add(hpProduct6);
        hpProductList.add(hpProduct7);
        hpProductList.add(hpProduct8);
        hpProductList.add(hpProduct9);
        hpProductList.add(hpProduct10);
        
        productCatalog.setHpProductList(hpProductList);
        
    //Toshiba
        Product toshibaProduct1 = new Product();
        toshibaProduct1.setProductName("T1");
        toshibaProduct1.setProductInfo("T1 Info");
        Product toshibaProduct2 = new Product();
        toshibaProduct2.setProductName("T2");
        toshibaProduct2.setProductInfo("T2 Info");
        Product toshibaProduct3 = new Product();
        toshibaProduct3.setProductName("T3");
        toshibaProduct3.setProductInfo("T3 Info");
        Product toshibaProduct4 = new Product();
        toshibaProduct4.setProductName("T4");
        toshibaProduct4.setProductInfo("T4 Info");
        Product toshibaProduct5 = new Product();
        toshibaProduct5.setProductName("T5");
        toshibaProduct5.setProductInfo("T5 Info");
        Product toshibaProduct6 = new Product();
        toshibaProduct6.setProductName("T6");
        toshibaProduct6.setProductInfo("T6 Info");
        Product toshibaProduct7 = new Product();
        toshibaProduct7.setProductName("T7");
        toshibaProduct7.setProductInfo("T7 Info");
        Product toshibaProduct8 = new Product();
        toshibaProduct8.setProductName("T8");
        toshibaProduct8.setProductInfo("T8 Info");
        Product toshibaProduct9 = new Product();
        toshibaProduct9.setProductName("T9");
        toshibaProduct9.setProductInfo("T9 Info");
        Product toshibaProduct10 = new Product();
        toshibaProduct10.setProductName("T10");
        toshibaProduct10.setProductInfo("T10 Info");
        
        toshibaProductList.add(toshibaProduct1);
        toshibaProductList.add(toshibaProduct2);
        toshibaProductList.add(toshibaProduct3);
        toshibaProductList.add(toshibaProduct4);
        toshibaProductList.add(toshibaProduct5);
        toshibaProductList.add(toshibaProduct6);
        toshibaProductList.add(toshibaProduct7);
        toshibaProductList.add(toshibaProduct8);
        toshibaProductList.add(toshibaProduct9);
        toshibaProductList.add(toshibaProduct10);
        
        productCatalog.setToshibaProductList(toshibaProductList);
        
    //Apple
        Product appleProduct1 = new Product();
        appleProduct1.setProductName("A1");
        appleProduct1.setProductInfo("A1 Info");
        Product appleProduct2 = new Product();
        appleProduct2.setProductName("A2");
        appleProduct2.setProductInfo("A2 Info");
        Product appleProduct3 = new Product();
        appleProduct3.setProductName("A3");
        appleProduct3.setProductInfo("A3 Info");
        Product appleProduct4 = new Product();
        appleProduct4.setProductName("A4");
        appleProduct4.setProductInfo("A4 Info");
        Product appleProduct5 = new Product();
        appleProduct5.setProductName("A5");
        appleProduct5.setProductInfo("A5 Info");
        Product appleProduct6 = new Product();
        appleProduct6.setProductName("A6");
        appleProduct6.setProductInfo("A6 Info");
        Product appleProduct7 = new Product();
        appleProduct7.setProductName("A7");
        appleProduct7.setProductInfo("A7 Info");
        Product appleProduct8 = new Product();
        appleProduct8.setProductName("A8");
        appleProduct8.setProductInfo("A8 Info");
        Product appleProduct9 = new Product();
        appleProduct9.setProductName("A9");
        appleProduct9.setProductInfo("A9 Info");
        Product appleProduct10 = new Product();
        appleProduct10.setProductName("A10");
        appleProduct10.setProductInfo("A10 Info");
        
        appleProductList.add(appleProduct1);
        appleProductList.add(appleProduct2);
        appleProductList.add(appleProduct3);
        appleProductList.add(appleProduct4);
        appleProductList.add(appleProduct5);
        appleProductList.add(appleProduct6);
        appleProductList.add(appleProduct7);
        appleProductList.add(appleProduct8);
        appleProductList.add(appleProduct9);
        appleProductList.add(appleProduct10);
        
        productCatalog.setAppleProductList(appleProductList);
        
    //Lenovo
        Product lenovoProduct1 = new Product();
        lenovoProduct1.setProductName("L1");
        lenovoProduct1.setProductInfo("L1 Info");
        Product lenovoProduct2 = new Product();
        lenovoProduct2.setProductName("L2");
        lenovoProduct2.setProductInfo("L2 Info");
        Product lenovoProduct3 = new Product();
        lenovoProduct3.setProductName("L3");
        lenovoProduct3.setProductInfo("L3 Info");
        Product lenovoProduct4 = new Product();
        lenovoProduct4.setProductName("L4");
        lenovoProduct4.setProductInfo("L4 Info");
        Product lenovoProduct5 = new Product();
        lenovoProduct5.setProductName("L5");
        lenovoProduct5.setProductInfo("L5 Info");
        Product lenovoProduct6 = new Product();
        lenovoProduct6.setProductName("L6");
        lenovoProduct6.setProductInfo("L6 Info");
        Product lenovoProduct7 = new Product();
        lenovoProduct7.setProductName("L7");
        lenovoProduct7.setProductInfo("L7 Info");
        Product lenovoProduct8 = new Product();
        lenovoProduct8.setProductName("L8");
        lenovoProduct8.setProductInfo("L8 Info");
        Product lenovoProduct9 = new Product();
        lenovoProduct9.setProductName("L9");
        lenovoProduct9.setProductInfo("L9 Info");
        Product lenovoProduct10 = new Product();
        lenovoProduct10.setProductName("L10");
        lenovoProduct10.setProductInfo("L10 Info");
        
        lenovoProductList.add(lenovoProduct1);
        lenovoProductList.add(lenovoProduct2);
        lenovoProductList.add(lenovoProduct3);
        lenovoProductList.add(lenovoProduct4);
        lenovoProductList.add(lenovoProduct5);
        lenovoProductList.add(lenovoProduct6);
        lenovoProductList.add(lenovoProduct7);
        lenovoProductList.add(lenovoProduct8);
        lenovoProductList.add(lenovoProduct9);
        lenovoProductList.add(lenovoProduct10);
        
        productCatalog.setLenovoProductList(lenovoProductList);
        
//Add all information to Catalog & Suppliers
        dell.setProductCatalog(productCatalog);
        hp.setProductCatalog(productCatalog);
        toshiba.setProductCatalog(productCatalog);
        apple.setProductCatalog(productCatalog);
        lenovo.setProductCatalog(productCatalog);
        
        suppliersList.add(dell);
        suppliersList.add(hp);
        suppliersList.add(toshiba);
        suppliersList.add(apple);
        suppliersList.add(lenovo);
        
        supplierDirectory.setListOfSuppliers(suppliersList);
        
    }
    
    public static void main(String arg[]){
        Main mainfunction = new Main();
        System.out.println(mainfunction.supplierDirectory.getListOfSuppliers());
        
    }
    
}


