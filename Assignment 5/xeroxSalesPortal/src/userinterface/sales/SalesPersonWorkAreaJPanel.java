/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.sales;

import business.Business;
import business.Customer;
import business.Order;
import business.UserAccount;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author ZappyZhao
 */
public class SalesPersonWorkAreaJPanel extends javax.swing.JPanel {
    private JPanel containerJPanel;
    private Business business;
    private UserAccount userAccount;
    private Customer customer;
    //private Order order;
    /**
     * Creates new form SalesPersonWorkAreaJPanel
     */
    public SalesPersonWorkAreaJPanel(JPanel containerJPanel, Business business, Customer customer, UserAccount userAccount) {
        initComponents();
        this.containerJPanel = containerJPanel;
        this.business = business;
        this.customer = customer;
        this.userAccount = userAccount;
        //this.order = order;
        
        spIDjLabel.setText(String.valueOf(userAccount.getSalesPerson().getSalesPersonID()));
        populateCustomerCombo();
    }

    public void populateCustomerCombo() {
        customerjComboBox.removeAllItems();
        for(Customer customer : business.getCustomerDirectory().getListOfCustomers()) {
            customerjComboBox.addItem(customer);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        customerjComboBox = new javax.swing.JComboBox();
        newCustomerjButton = new javax.swing.JButton();
        gojButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        spIDjLabel = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel1.setText("Sales Person Work Area");

        jLabel2.setText("Choose a Customer");

        customerjComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        newCustomerjButton.setText("New Customer");
        newCustomerjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newCustomerjButtonActionPerformed(evt);
            }
        });

        gojButton.setText("Start Shopping");
        gojButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gojButtonActionPerformed(evt);
            }
        });

        backjButton.setText("<<Back");
        backjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjButtonActionPerformed(evt);
            }
        });

        jLabel3.setText("Sales Person ID:");

        spIDjLabel.setText("jLabel4");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(newCustomerjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(69, 69, 69)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(backjButton)
                                    .addGap(173, 173, 173)
                                    .addComponent(gojButton))
                                .addComponent(jLabel1)))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(186, 186, 186)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3))
                            .addGap(66, 66, 66)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(spIDjLabel)
                                .addComponent(customerjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(248, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(spIDjLabel))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(customerjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(newCustomerjButton)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(gojButton)
                    .addComponent(backjButton))
                .addContainerGap(147, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void gojButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gojButtonActionPerformed
        Customer customer = (Customer)customerjComboBox.getSelectedItem();
        SalePageJPanel spjp = new SalePageJPanel(containerJPanel, business, customer, userAccount);
        containerJPanel.add("SalePage", spjp);
        CardLayout cardLayout = (CardLayout)containerJPanel.getLayout();
        cardLayout.next(containerJPanel);
    }//GEN-LAST:event_gojButtonActionPerformed

    private void backjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjButtonActionPerformed
        containerJPanel.remove(this);
        CardLayout cardLayout = (CardLayout)containerJPanel.getLayout();
        cardLayout.previous(containerJPanel);
    }//GEN-LAST:event_backjButtonActionPerformed

    private void newCustomerjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newCustomerjButtonActionPerformed
        AddCustomerJPanel acjp = new AddCustomerJPanel(containerJPanel, business, this);
        containerJPanel.add("AddCustomer", acjp);
        CardLayout cardLayout = (CardLayout)containerJPanel.getLayout();
        cardLayout.next(containerJPanel);
    }//GEN-LAST:event_newCustomerjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JComboBox customerjComboBox;
    private javax.swing.JButton gojButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton newCustomerjButton;
    private javax.swing.JLabel spIDjLabel;
    // End of variables declaration//GEN-END:variables
}
