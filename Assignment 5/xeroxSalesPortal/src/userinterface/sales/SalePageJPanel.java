/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.sales;

import business.Business;
import business.Customer;
import business.Order;
import business.OrderItem;
import business.Product;
import business.UserAccount;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ZappyZhao
 */
public class SalePageJPanel extends javax.swing.JPanel {
    private JPanel containerJPanel;
    private Business business;
    private Customer customer;
    private UserAccount userAccount;
    Order order;
    boolean isCheckedOut = false;
    /**
     * Creates new form SalePageJPanel
     */
    public SalePageJPanel(JPanel containerJPanel, Business business, Customer customer, UserAccount userAccount) {
        initComponents();
        this.containerJPanel = containerJPanel;
        this.business = business;
        this.customer = customer;
        this.userAccount = userAccount;
        //this.order = order;
        
        spIDjLabel.setText(String.valueOf(userAccount.getSalesPerson().getSalesPersonID()));
        cIDjLabel.setText(String.valueOf(customer.getCustomerID()));
        
        populateProductCatalogJTable();
        
        
        ////make new order:
        if(isCheckedOut == false) {
            order = new Order();
            populateOrderJTable();
            orderNumberjLabel.setText(String.valueOf(order.getOrderNumber()));
        }
    }

    public void populateProductCatalogJTable() {
        DefaultTableModel dtm = (DefaultTableModel)productCatalogjTable.getModel();
        dtm.setRowCount(0);
        for(Product product : business.getProductCatalog().getListOfProducts()) {
            Object row[] = new Object[5];
            row[0] = product;
            row[1] = product.getPriceFloor();
            row[2] = product.getPriceCeiling();
            row[3] = product.getPriceTarget();
            row[4] = product.getAvailableQuantity();
            dtm.addRow(row);
        }
        
    }
    ///// Set Order Table of just Order, if checked out, add order to ordercatalog, sp'sOrder, and customer's Order
    public void populateOrderJTable() {
        DefaultTableModel dtm = (DefaultTableModel)orderjTable.getModel();
        dtm.setRowCount(0);
        for(OrderItem orderItem : order.getListOfItems()) {
            Object row[] = new Object[4];
            row[0] = orderItem;
            row[1] = orderItem.getSalePrice();
            row[2] = orderItem.getOrderQuantity();
            row[3] = orderItem.getSalePrice() * orderItem.getOrderQuantity();
            dtm.addRow(row);
        }
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        productCatalogjTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        salePricejTextField = new javax.swing.JTextField();
        addToCartjButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        orderjTable = new javax.swing.JTable();
        backjButton = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        quantityjSpinner = new javax.swing.JSpinner();
        addItemjButton = new javax.swing.JButton();
        deletejButton = new javax.swing.JButton();
        checkOutjButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        spIDjLabel = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cIDjLabel = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        orderNumberjLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Chalkboard", 1, 20)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userinterface/sales/xerox.jpeg")));
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("Welcome to Xerox!");

        productCatalogjTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Price_floor", "Price_ceiling", "Price_target", "Available Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(productCatalogjTable);

        jLabel2.setText("Sale Price:");

        addToCartjButton.setText("Add To Cart");
        addToCartjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addToCartjButtonActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel3.setText("Order:");

        orderjTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product", "Sale Price", "Order Quantity", "Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(orderjTable);

        backjButton.setText("<<Save and Quit this Order");
        backjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjButtonActionPerformed(evt);
            }
        });

        jLabel4.setText("Choose an item and Add Quantity");

        addItemjButton.setText("Add Item");
        addItemjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addItemjButtonActionPerformed(evt);
            }
        });

        deletejButton.setText("Delete");
        deletejButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletejButtonActionPerformed(evt);
            }
        });

        checkOutjButton.setText("Check Out");
        checkOutjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkOutjButtonActionPerformed(evt);
            }
        });

        jLabel5.setText("Sales Person ID:");

        spIDjLabel.setText("jLabel6");

        jLabel7.setText("Customer ID:");

        cIDjLabel.setText("jLabel8");

        jLabel6.setText("Order NUmber:");

        orderNumberjLabel.setText("jLabel8");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(288, 288, 288)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(salePricejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(72, 72, 72)
                        .addComponent(addToCartjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(quantityjSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(43, 43, 43)
                                .addComponent(addItemjButton)
                                .addGap(18, 18, 18)
                                .addComponent(deletejButton)
                                .addGap(31, 31, 31)
                                .addComponent(checkOutjButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 759, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel5)
                                .addGap(27, 27, 27)
                                .addComponent(spIDjLabel)
                                .addGap(36, 36, 36)
                                .addComponent(jLabel7)
                                .addGap(29, 29, 29)
                                .addComponent(cIDjLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel6)
                                .addGap(79, 79, 79)
                                .addComponent(orderNumberjLabel))
                            .addComponent(jScrollPane2)
                            .addComponent(backjButton))))
                .addContainerGap(41, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(spIDjLabel)
                        .addComponent(jLabel7)
                        .addComponent(cIDjLabel))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(salePricejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addToCartjButton))
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(orderNumberjLabel))
                .addGap(27, 27, 27)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(quantityjSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addItemjButton)
                    .addComponent(deletejButton)
                    .addComponent(checkOutjButton))
                .addGap(18, 18, 18)
                .addComponent(backjButton)
                .addContainerGap(33, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjButtonActionPerformed
        if(isCheckedOut == false) {
            int warning = (Integer)JOptionPane.showConfirmDialog(this, "Do you want to quit without check out?", "Warning", JOptionPane.YES_NO_OPTION);
            if(warning == JOptionPane.YES_OPTION) {
                ///// add back product quantity
                for(OrderItem oi : order.getListOfItems()) {
                    oi.getProduct().setAvailableQuantity(oi.getProduct().getAvailableQuantity() + oi.getOrderQuantity());
                }
                order.getListOfItems().removeAll(order.getListOfItems());
                containerJPanel.remove(this);
                CardLayout cardLayout = (CardLayout)containerJPanel.getLayout();
                cardLayout.previous(containerJPanel);
            }
        }
        else if(isCheckedOut == true) {
            containerJPanel.remove(this);
            CardLayout cardLayout = (CardLayout)containerJPanel.getLayout();
            cardLayout.previous(containerJPanel);    
        }
        
    }//GEN-LAST:event_backjButtonActionPerformed

    private void addToCartjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addToCartjButtonActionPerformed
        int selectedRow = productCatalogjTable.getSelectedRow();
        float salePrice;
        boolean alreadyPresent = false;
        if(selectedRow>=0) {
            Product selectedProduct = (Product)productCatalogjTable.getValueAt(selectedRow, 0);
            try{
                salePrice = Float.parseFloat(salePricejTextField.getText());
            }catch(Exception e) {
                JOptionPane.showMessageDialog(this, "Please Enter Valid Sales Price", "Warning", JOptionPane.WARNING_MESSAGE);
                salePricejTextField.setText("");
                return;
            }
            ///// The price should < ceiling && > floor
            if(salePrice < selectedProduct.getPriceFloor()){
                JOptionPane.showMessageDialog(this, "Please Enter Larger Sales Price", "Warning", JOptionPane.WARNING_MESSAGE);
                salePricejTextField.setText("");
            }
            else if(salePrice > selectedProduct.getPriceCeiling()) {
                JOptionPane.showMessageDialog(this, "Please Enter Lower Sales Price", "Warning", JOptionPane.WARNING_MESSAGE);
                salePricejTextField.setText("");
            }
            else {
              /////check if already exists:  
                for(OrderItem oi : order.getListOfItems()) {
                    if(selectedProduct.getProductName() == oi.getProduct().getProductName()) {
                        alreadyPresent = true;
                    }
                    else
                        alreadyPresent = false;
                }
                if(alreadyPresent == true){
                    JOptionPane.showMessageDialog(this, "This item already exists in order table", "Warning", JOptionPane.WARNING_MESSAGE);
                    salePricejTextField.setText("");
                }                
                else if(alreadyPresent == false) {
        /////// Make the order and add to cart
                OrderItem orderItem = order.addNewItems();
                orderItem.setProduct(selectedProduct);
                orderItem.setInterest(salePrice-selectedProduct.getPriceFloor());
                orderItem.setOrderQuantity(1);
                orderItem.setSalePrice(salePrice);
                if(salePrice > selectedProduct.getPriceTarget()) {
                    orderItem.setAboveFlag(true);
                    orderItem.setBelowFlag(false);
                }
                else if(salePrice < selectedProduct.getPriceTarget()) {
                    orderItem.setAboveFlag(false);
                    orderItem.setBelowFlag(true);
                }
                else {
                    orderItem.setAboveFlag(false);
                    orderItem.setBelowFlag(false);
                }
                salePricejTextField.setText("");
                populateOrderJTable();
                
        //// Reduce Available Quantity
                selectedProduct.setAvailableQuantity(selectedProduct.getAvailableQuantity() - 1);
                populateProductCatalogJTable();
                }
            }
            
        }
        else {
            JOptionPane.showMessageDialog(this, "Please Select a row", "Warning", JOptionPane.WARNING_MESSAGE);
            
        }
        
        
    }//GEN-LAST:event_addToCartjButtonActionPerformed

    private void deletejButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletejButtonActionPerformed
        int selectedRow = orderjTable.getSelectedRow();
        if(selectedRow>=0) {
            int warningDialog = JOptionPane.showConfirmDialog(this,"Would you like to delete this item?","Warning",JOptionPane.YES_NO_OPTION);
            if(warningDialog == JOptionPane.YES_OPTION){
                OrderItem oi = (OrderItem)orderjTable.getValueAt(selectedRow, 0);
                order.deleteItem(oi);
                populateOrderJTable();
                oi.getProduct().setAvailableQuantity(oi.getProduct().getAvailableQuantity() + oi.getOrderQuantity());
                populateProductCatalogJTable();
            }
        }
        else
            JOptionPane.showMessageDialog(this, "Please select a row", "Warning", JOptionPane.WARNING_MESSAGE);
    }//GEN-LAST:event_deletejButtonActionPerformed

    private void addItemjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addItemjButtonActionPerformed
        int selectedRow = orderjTable.getSelectedRow();
        if(selectedRow>=0) {
            int oldQuantity = (Integer)orderjTable.getValueAt(selectedRow, 2);
            int addQuantity = (Integer)quantityjSpinner.getValue();
            if(addQuantity<=0) {
                JOptionPane.showMessageDialog(this, "Please choose a valid quantity", "Warning", JOptionPane.WARNING_MESSAGE);
                return;    ///////////////!!!!!!!!!!!!!!!!!!!!check
            }
            else {
                int warningDialog = JOptionPane.showConfirmDialog(this,"Would you like to add quantity?","Warning",JOptionPane.YES_NO_OPTION);
                if(warningDialog == JOptionPane.YES_OPTION){
                    OrderItem oi = (OrderItem)orderjTable.getValueAt(selectedRow, 0);
                    oi.setOrderQuantity(oldQuantity+addQuantity);
                    populateOrderJTable();
                    quantityjSpinner.setValue(0);
                    oi.getProduct().setAvailableQuantity(oi.getProduct().getAvailableQuantity()-addQuantity);
                    populateProductCatalogJTable();
                }
            }
        }
        else
            JOptionPane.showMessageDialog(this, "Please select a row", "Warning", JOptionPane.WARNING_MESSAGE);
    }//GEN-LAST:event_addItemjButtonActionPerformed

    private void checkOutjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkOutjButtonActionPerformed
        if(order.getListOfItems().size()>0) {
           int warningDialog = JOptionPane.showConfirmDialog(this,"Would you like to check out?","Warning",JOptionPane.YES_NO_OPTION);
           if(warningDialog == JOptionPane.YES_OPTION){
               // Add order to OrderCatalog\SalesPerson Order\Customer Order
               business.getOrderCatalog().addOrders(order);
               userAccount.getSalesPerson().setOrder(order);
               customer.setOrder(order);
               
               
            ///// for salesPerson, set countAbove&Below
               for(OrderItem checkingOI : order.getListOfItems()){
                   if(checkingOI.isAboveFlag() == true) {
                       userAccount.getSalesPerson().setCountAbove(userAccount.getSalesPerson().getCountAbove() + checkingOI.getOrderQuantity());
                   }
                   if(checkingOI.isBelowFlag() == true) {
                       userAccount.getSalesPerson().setCountBelow(userAccount.getSalesPerson().getCountBelow() + checkingOI.getOrderQuantity());
                   }
               }
            
            ///// for salesPerson, in order to rank, set saleQuantity:
               for(OrderItem checkingOI1 : order.getListOfItems()) {
                   userAccount.getSalesPerson().setSaleQuantity(userAccount.getSalesPerson().getSaleQuantity() + checkingOI1.getOrderQuantity());
               }
            ///// for salesPerson, in order to rank, set saleProfits: set commission:
               for(OrderItem checkingOI2 : order.getListOfItems()) {
                   userAccount.getSalesPerson().setSaleProfits(userAccount.getSalesPerson().getSaleProfits() +
                           checkingOI2.getInterest()*checkingOI2.getOrderQuantity());
                   userAccount.getSalesPerson().setCommission(userAccount.getSalesPerson().getCommission() + 
                           checkingOI2.getInterest()*checkingOI2.getOrderQuantity()/10);
               }
            
            ///// for customer, in order to rank, set consumption
               for(OrderItem checkingOI3 : order.getListOfItems()) {
                   customer.setTotalConsumption(customer.getTotalConsumption() + checkingOI3.getSalePrice()*checkingOI3.getOrderQuantity());
               }
               
        
               isCheckedOut=true;
               addToCartjButton.setEnabled(false);
               salePricejTextField.setEnabled(false);
               addItemjButton.setEnabled(false);
               deletejButton.setEnabled(false);
               checkOutjButton.setEnabled(false);
               JOptionPane.showMessageDialog(this, "Make Sure to click Back btn to save the order!", "Success!", JOptionPane.PLAIN_MESSAGE);
           }            
        }
        else {
            JOptionPane.showMessageDialog(this, "No Items available in the cart","Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_checkOutjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addItemjButton;
    private javax.swing.JButton addToCartjButton;
    private javax.swing.JButton backjButton;
    private javax.swing.JLabel cIDjLabel;
    private javax.swing.JButton checkOutjButton;
    private javax.swing.JButton deletejButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel orderNumberjLabel;
    private javax.swing.JTable orderjTable;
    private javax.swing.JTable productCatalogjTable;
    private javax.swing.JSpinner quantityjSpinner;
    private javax.swing.JTextField salePricejTextField;
    private javax.swing.JLabel spIDjLabel;
    // End of variables declaration//GEN-END:variables
}
