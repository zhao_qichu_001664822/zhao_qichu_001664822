/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.admin;

import business.Business;
import business.SalesPerson;
import business.UserAccount;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author ZappyZhao
 */
public class AddUserAccountJPanel extends javax.swing.JPanel {
    private JPanel containerJPanel;
    private Business business;
    private ManageUserAccountJPanel muajp;
    /**
     * Creates new form AddUserAccountJPanel
     */
    public AddUserAccountJPanel(JPanel containerJPanel, Business business, ManageUserAccountJPanel muajp) {
        initComponents();
        this.containerJPanel = containerJPanel;
        this.business = business;
        this.muajp = muajp;
        populatePersonCombo();
        populateRoleCombo();
        
    }

    public void populatePersonCombo() {
        personjComboBox.removeAllItems();
        for(SalesPerson salesPerson : business.getSalesPersonDirectory().getListOfSalesPerson()) {
            if(!salesPerson.getSalesPersonName().equals("admin")){
                personjComboBox.addItem(salesPerson);
            }
        }
    }
    
    public void populateRoleCombo() {
        rolejComboBox.removeAllItems();
        rolejComboBox.addItem(UserAccount.SalesPerson_ROLE);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel3 = new javax.swing.JLabel();
        passwordjTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        userNamejTextField = new javax.swing.JTextField();
        activejRadioButton = new javax.swing.JRadioButton();
        disablejRadioButton = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        personjComboBox = new javax.swing.JComboBox();
        rolejComboBox = new javax.swing.JComboBox();
        backjButton = new javax.swing.JButton();
        createjButton = new javax.swing.JButton();

        jLabel3.setText("Password");

        jLabel1.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel1.setText("Create User Account");

        jLabel2.setText("Username");

        buttonGroup1.add(activejRadioButton);
        activejRadioButton.setSelected(true);
        activejRadioButton.setText("is Active");

        buttonGroup1.add(disablejRadioButton);
        disablejRadioButton.setText("Disable");

        jLabel4.setText("Person:");

        jLabel5.setText("Role:");

        personjComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        rolejComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        backjButton.setText("<<Back");
        backjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjButtonActionPerformed(evt);
            }
        });

        createjButton.setText("Create");
        createjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backjButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(createjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(activejRadioButton)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(disablejRadioButton))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel5))
                                    .addGap(48, 48, 48)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(userNamejTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                                        .addComponent(passwordjTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                                        .addComponent(personjComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(rolejComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))))
                .addGap(296, 296, 296))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel1)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(userNamejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(passwordjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(activejRadioButton)
                    .addComponent(disablejRadioButton))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(personjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rolejComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backjButton)
                    .addComponent(createjButton))
                .addGap(21, 21, 21))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjButtonActionPerformed
        containerJPanel.remove(this);
        muajp.populateUserAccountJTable();
        CardLayout cardLayout = (CardLayout)containerJPanel.getLayout();
        cardLayout.previous(containerJPanel);
    }//GEN-LAST:event_backjButtonActionPerformed

    private void createjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createjButtonActionPerformed
        String username = userNamejTextField.getText().trim();
        String password = passwordjTextField.getText().trim();
        boolean fine = true;
        // Check no blanks:
        if(username.isEmpty() || password.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please compelete all the blanks",
                            "Warning", JOptionPane.WARNING_MESSAGE);
        }
        else {
            // Check if username already exists:
            for(UserAccount userAccount : business.getUserAccountDirectory().getListOfUserAccount()) {
                if(username.equals(userAccount.getUsername())) {
                    JOptionPane.showMessageDialog(this, "This Username has already exists, Please change another",
                            "Warning", JOptionPane.WARNING_MESSAGE);
                    fine = false;
                    userNamejTextField.setText("");
                    passwordjTextField.setText("");
                }
            }
            // Check if selectedSalesPerson already have an active account:
            for(UserAccount userAccount : business.getUserAccountDirectory().getListOfUserAccount()) {
                if(userAccount.isIsActive() && 
                   userAccount.getSalesPerson().getSalesPersonName().equals(String.valueOf(personjComboBox.getSelectedItem()))) {
                    JOptionPane.showMessageDialog(this, "Your account has already exists",
                        "Error", JOptionPane.ERROR_MESSAGE);
                    fine = false;
                    userNamejTextField.setText("");
                    passwordjTextField.setText("");
                }
            }
            // Create new account:
            if(fine) {
                UserAccount newua = business.getUserAccountDirectory().addNewUserAccount();
                newua.setUsername(username);
                newua.setPassword(password);
                newua.setSalesPerson((SalesPerson)personjComboBox.getSelectedItem());
                newua.setRole(String.valueOf(rolejComboBox.getSelectedItem()));
                
                if(activejRadioButton.isSelected()) {
                    newua.setIsActive(true);
                }
                else{
                    newua.setIsActive(false);
                }
                JOptionPane.showMessageDialog(this, "Account added successfully", "Success", JOptionPane.PLAIN_MESSAGE);
                
                userNamejTextField.setText("");
                passwordjTextField.setText("");
            }
        }
    }//GEN-LAST:event_createjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton activejRadioButton;
    private javax.swing.JButton backjButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton createjButton;
    private javax.swing.JRadioButton disablejRadioButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField passwordjTextField;
    private javax.swing.JComboBox personjComboBox;
    private javax.swing.JComboBox rolejComboBox;
    private javax.swing.JTextField userNamejTextField;
    // End of variables declaration//GEN-END:variables
}
