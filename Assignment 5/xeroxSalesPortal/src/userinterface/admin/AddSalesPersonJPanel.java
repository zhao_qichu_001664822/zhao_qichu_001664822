/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.admin;

import business.Business;
import business.SalesPerson;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author ZappyZhao
 */
public class AddSalesPersonJPanel extends javax.swing.JPanel {
    private JPanel containerJPanel;
    private Business business;
    private ManageSalesPersonJPanel manageSalesPersonJPanel;
    /**
     * Creates new form AddSalesPersonJPanel
     */
    public AddSalesPersonJPanel(JPanel containerJPanel, Business business, ManageSalesPersonJPanel manageSalesPersonJPanel) {
        initComponents();
        this.containerJPanel = containerJPanel;
        this.business = business;
        this.manageSalesPersonJPanel = manageSalesPersonJPanel;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        nameSPjTextField = new javax.swing.JTextField();
        backjButton = new javax.swing.JButton();
        createjButton = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel1.setText("Add Sales Person");

        jLabel2.setText("Name");

        backjButton.setText("<<Back");
        backjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjButtonActionPerformed(evt);
            }
        });

        createjButton.setText("Create");
        createjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(177, 177, 177)
                        .addComponent(jLabel2)
                        .addGap(89, 89, 89)
                        .addComponent(nameSPjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(backjButton)
                                .addGap(199, 199, 199)
                                .addComponent(createjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1))))
                .addContainerGap(326, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel1)
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nameSPjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(70, 70, 70)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backjButton)
                    .addComponent(createjButton))
                .addContainerGap(216, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjButtonActionPerformed
        containerJPanel.remove(this);
        manageSalesPersonJPanel.populateSalesPersonJTable();
        CardLayout cardLayout = (CardLayout)containerJPanel.getLayout();
        cardLayout.previous(containerJPanel);
    }//GEN-LAST:event_backjButtonActionPerformed

    private void createjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createjButtonActionPerformed
        String name = nameSPjTextField.getText().trim();
        if(name.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please Complete all blanks", "Error",JOptionPane.ERROR_MESSAGE);
        }
        else {
            SalesPerson sp = business.getSalesPersonDirectory().addNewSalesPerson();
            sp.setSalesPersonName(name);
            JOptionPane.showMessageDialog(this, "New Sales Person Information has already created!", "Success!", JOptionPane.PLAIN_MESSAGE);
            nameSPjTextField.setText("");
        }
    }//GEN-LAST:event_createjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JButton createjButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField nameSPjTextField;
    // End of variables declaration//GEN-END:variables
}
