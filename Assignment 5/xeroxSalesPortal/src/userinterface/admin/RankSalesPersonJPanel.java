/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.admin;

import business.Business;
import business.SalesPerson;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ZappyZhao
 */
public class RankSalesPersonJPanel extends javax.swing.JPanel {
    private JPanel containerJPanel;
    private Business business;
    /**
     * Creates new form RankSalesPersonJPanel
     */
    public RankSalesPersonJPanel(JPanel containerJPanel, Business business) {
        initComponents();
        this.containerJPanel = containerJPanel;
        this.business = business;
        populateQuantityJTable();
        populateProfitJTable();
    }
    
    public void populateQuantityJTable() {
    ////// Make Order
        // Initialize RankQ to the smallest Number (First one)
       //loop and order
        for(int i=0; i<business.getSalesPersonDirectory().getListOfSalesPerson().size(); i++) {
            int count = 1;
            for(int j=0; j<business.getSalesPersonDirectory().getListOfSalesPerson().size(); j++) {
                if(business.getSalesPersonDirectory().getListOfSalesPerson().get(i).getSaleQuantity() < 
                   business.getSalesPersonDirectory().getListOfSalesPerson().get(j).getSaleQuantity()) {
                    count++;
                    business.getSalesPersonDirectory().getListOfSalesPerson().get(i).setRankQuantity(count);
                }
                else {
                    business.getSalesPersonDirectory().getListOfSalesPerson().get(i).setRankQuantity(count);
                }
            }
        }
        
    ////// Design Table    
        DefaultTableModel dtm = (DefaultTableModel)rankSalesQuantityjTable.getModel();
        dtm.setRowCount(0);
        
        for(int a=1; a<=business.getSalesPersonDirectory().getListOfSalesPerson().size(); a++) {
            for(SalesPerson salesPerson : business.getSalesPersonDirectory().getListOfSalesPerson()) {
                if(salesPerson.getRankQuantity() == a) {
                    Object row[] = new Object[4];
                    row[0] = salesPerson.getRankQuantity();
                    row[1] = salesPerson;
                    row[2] = salesPerson.getSalesPersonID();
                    row[3] = salesPerson.getSaleQuantity();
                    dtm.addRow(row);
                    //break;
                }
            }

        }
    }
    public void populateProfitJTable() {
        ////// Make Order
        // Initialize RankP to the smallest Number (First one)
       //loop and order
        for(int x=0; x<business.getSalesPersonDirectory().getListOfSalesPerson().size(); x++) {
            int tempt = 1;
            for(int y=0; y<business.getSalesPersonDirectory().getListOfSalesPerson().size(); y++) {
                if(business.getSalesPersonDirectory().getListOfSalesPerson().get(x).getSaleProfits()< 
                   business.getSalesPersonDirectory().getListOfSalesPerson().get(y).getSaleProfits()) {
                    tempt++;
                    business.getSalesPersonDirectory().getListOfSalesPerson().get(x).setRankProfits(tempt);
                }
                else {
                    business.getSalesPersonDirectory().getListOfSalesPerson().get(x).setRankProfits(tempt);
                }
            }
        }
        
    ////// Design Table    
        DefaultTableModel dtm = (DefaultTableModel)rankSalesProfitsjTable.getModel();
        dtm.setRowCount(0);
        
        for(int b=1; b<=business.getSalesPersonDirectory().getListOfSalesPerson().size(); b++) {
            for(SalesPerson salesPerson : business.getSalesPersonDirectory().getListOfSalesPerson()) {
                if(salesPerson.getRankProfits()== b) {
                    Object row[] = new Object[4];
                    row[0] = salesPerson.getRankProfits();
                    row[1] = salesPerson;
                    row[2] = salesPerson.getSalesPersonID();
                    row[3] = salesPerson.getSaleProfits();
                    dtm.addRow(row);
                }
            }

        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        rankSalesProfitsjTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        rankSalesQuantityjTable = new javax.swing.JTable();
        backjButton = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel1.setText("Rank of Sales Person");

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 15)); // NOI18N
        jLabel2.setText("Rank by Sales Quantity");

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 1, 15)); // NOI18N
        jLabel3.setText("Rank by Sales Profits");

        rankSalesProfitsjTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Rank", "Name", "ID", "Total Profits"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(rankSalesProfitsjTable);

        rankSalesQuantityjTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Rank", "Name", "ID", "Total Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(rankSalesQuantityjTable);

        backjButton.setText("<<Back");
        backjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(335, 335, 335)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 824, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 824, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(339, 339, 339)
                        .addComponent(jLabel3)))
                .addContainerGap(46, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(backjButton)
                .addContainerGap(53, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjButtonActionPerformed
        containerJPanel.remove(this);
        CardLayout cardLayout = (CardLayout)containerJPanel.getLayout();
        cardLayout.previous(containerJPanel);
    }//GEN-LAST:event_backjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable rankSalesProfitsjTable;
    private javax.swing.JTable rankSalesQuantityjTable;
    // End of variables declaration//GEN-END:variables
}
