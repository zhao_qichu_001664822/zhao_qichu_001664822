/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author ZappyZhao
 */
public class Product {
    private String productName;
    private int priceFloor;
    private int priceCeiling;
    private int priceTarget;
    
    private int availableQuantity;
    private int soldQuantity;
    private int rankSoldQ;

    public int getRankSoldQ() {
        return rankSoldQ;
    }

    public void setRankSoldQ(int rankSoldQ) {
        this.rankSoldQ = rankSoldQ;
    }
    
    public int getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(int soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }
    
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPriceFloor() {
        return priceFloor;
    }

    public void setPriceFloor(int priceFloor) {
        this.priceFloor = priceFloor;
    }

    public int getPriceCeiling() {
        return priceCeiling;
    }

    public void setPriceCeiling(int priceCeiling) {
        this.priceCeiling = priceCeiling;
    }

    public int getPriceTarget() {
        return priceTarget;
    }

    public void setPriceTarget(int priceTarget) {
        this.priceTarget = priceTarget;
    }

    @Override
    public String toString() {
        return productName;
    }
    
    
    
    
}
