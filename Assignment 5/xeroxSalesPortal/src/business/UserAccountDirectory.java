/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class UserAccountDirectory {
    private ArrayList<UserAccount> listOfUserAccount;
    
    public UserAccountDirectory() {
        listOfUserAccount = new ArrayList<UserAccount>();
    }
    
    public void deleteUserAccount(UserAccount deleteUA) {
        listOfUserAccount.remove(deleteUA);
    }
    
    public UserAccount addNewUserAccount(){
        UserAccount u = new UserAccount();
        listOfUserAccount.add(u);
        return u;
    }

    public ArrayList<UserAccount> getListOfUserAccount() {
        return listOfUserAccount;
    }

    public void setListOfUserAccount(ArrayList<UserAccount> listOfUserAccount) {
        this.listOfUserAccount = listOfUserAccount;
    }
}
