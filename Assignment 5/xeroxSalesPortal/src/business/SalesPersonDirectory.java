/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class SalesPersonDirectory {
    private ArrayList<SalesPerson> listOfSalesPerson;

    public SalesPersonDirectory() {
        listOfSalesPerson = new ArrayList<SalesPerson>();
    }

    public ArrayList<SalesPerson> getListOfSalesPerson() {
        return listOfSalesPerson;
    }

    public void setListOfSalesPerson(ArrayList<SalesPerson> listOfSalesPerson) {
        this.listOfSalesPerson = listOfSalesPerson;
    }
    
/////// Add a salesPerson:    
    public SalesPerson addNewSalesPerson() {
        SalesPerson sp = new SalesPerson();
        listOfSalesPerson.add(sp);
        return sp;
    }
    
/////// Delete a salesPerson:
    public void deleteSalesPerson(SalesPerson deletesp) {
        listOfSalesPerson.remove(deletesp);
    }
    
    
}
