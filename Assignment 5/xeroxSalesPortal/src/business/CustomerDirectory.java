/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class CustomerDirectory {
    private ArrayList<Customer> listOfCustomers;

    public CustomerDirectory() {
        listOfCustomers = new ArrayList<Customer>();
    }

    public ArrayList<Customer> getListOfCustomers() {
        return listOfCustomers;
    }

    public void setListOfCustomers(ArrayList<Customer> listOfCustomers) {
        this.listOfCustomers = listOfCustomers;
    }
    
/////// Add a new customer
    public Customer addNewCustomer() {
        Customer c = new Customer();
        listOfCustomers.add(c);
        return c;
    }
    
}
