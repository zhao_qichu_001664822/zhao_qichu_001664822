/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class ProductCatalog {
    private ArrayList<Product> listOfProducts;

    public ProductCatalog() {
        listOfProducts = new ArrayList<Product>();
    }

    public ArrayList<Product> getListOfProducts() {
        return listOfProducts;
    }

    public void setListOfProducts(ArrayList<Product> listOfProducts) {
        this.listOfProducts = listOfProducts;
    }
    
////////Add new Product
    public Product addNewProduct() {
        Product p = new Product();
        listOfProducts.add(p);
        return p;
    }
    
}
