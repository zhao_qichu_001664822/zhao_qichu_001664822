/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author ZappyZhao
 */
public class SalesPerson {
    private String salesPersonName;
    private static int count = 0;
    private int salesPersonID;
    
    private float commission;
    private int countAbove;
    private int countBelow;
    private Order order;
    
    private int saleQuantity;
    private float saleProfits;
    private int rankQuantity;
    private int rankProfits;

    public float getSaleProfits() {
        return saleProfits;
    }

    public void setSaleProfits(float saleProfits) {
        this.saleProfits = saleProfits;
    }

    public int getRankProfits() {
        return rankProfits;
    }

    public void setRankProfits(int rankProfits) {
        this.rankProfits = rankProfits;
    }

    public SalesPerson() {
        salesPersonID = ++count;
    }

    public int getRankQuantity() {
        return rankQuantity;
    }

    public void setRankQuantity(int rankQuantity) {
        this.rankQuantity = rankQuantity;
    }
    
    public int getSaleQuantity() {
        return saleQuantity;
    }

    public void setSaleQuantity(int saleQuantity) {
        this.saleQuantity = saleQuantity;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
    
    public String getSalesPersonName() {
        return salesPersonName;
    }

    public void setSalesPersonName(String salesPersonName) {
        this.salesPersonName = salesPersonName;
    }

    public int getSalesPersonID() {
        return salesPersonID;
    }

    public void setSalesPersonID(int salesPersonID) {
        this.salesPersonID = salesPersonID;
    }

    public float getCommission() {
        return commission;
    }

    public void setCommission(float commission) {
        this.commission = commission;
    }

    public int getCountAbove() {
        return countAbove;
    }

    public void setCountAbove(int countAbove) {
        this.countAbove = countAbove;
    }

    public int getCountBelow() {
        return countBelow;
    }

    public void setCountBelow(int countBelow) {
        this.countBelow = countBelow;
    }

    @Override
    public String toString() {
        return salesPersonName;
    }
    
    
    
    
    
}
