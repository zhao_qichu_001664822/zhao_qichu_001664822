/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class OrderCatalog {
    private ArrayList<Order> listOfOrders;
    
    public OrderCatalog(){
        listOfOrders = new ArrayList<Order>();
    }

    public ArrayList<Order> getListOfOrders() {
        return listOfOrders;
    }

    public void setListOfOrders(ArrayList<Order> listOfOrders) {
        this.listOfOrders = listOfOrders;
    }

    public Order addOrders(Order order){
        listOfOrders.add(order);
        return order;
    }
    
}
