/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author ZappyZhao
 */
public class ConfigureBusiness {
    public static Business initializeBusiness() {
        Business business = Business.getInstance();
    
////// Initialize products: total of 5 products
        Product p1 = business.getProductCatalog().addNewProduct();
        p1.setProductName("Product A");
        p1.setPriceFloor(1);
        p1.setPriceCeiling(p1.getPriceFloor()*10);
        p1.setPriceTarget(p1.getPriceFloor()*5);
        p1.setAvailableQuantity(100);
        
        Product p2 = business.getProductCatalog().addNewProduct();
        p2.setProductName("Product B");
        p2.setPriceFloor(2);
        p2.setPriceCeiling(p2.getPriceFloor()*10);
        p2.setPriceTarget(p2.getPriceFloor()*5);
        p2.setAvailableQuantity(100);
        
        Product p3 = business.getProductCatalog().addNewProduct();
        p3.setProductName("Product C");
        p3.setPriceFloor(3);
        p3.setPriceCeiling(p3.getPriceFloor()*10);
        p3.setPriceTarget(p3.getPriceFloor()*5);
        p3.setAvailableQuantity(100);
        
        Product p4 = business.getProductCatalog().addNewProduct();
        p4.setProductName("Product D");
        p4.setPriceFloor(4);
        p4.setPriceCeiling(p4.getPriceFloor()*10);
        p4.setPriceTarget(p4.getPriceFloor()*5);
        p4.setAvailableQuantity(100);
        
        Product p5 = business.getProductCatalog().addNewProduct();
        p5.setProductName("Product E");
        p5.setPriceFloor(5);
        p5.setPriceCeiling(p5.getPriceFloor()*10);
        p5.setPriceTarget(p5.getPriceFloor()*5);
        p5.setAvailableQuantity(100);
        
        
////// Initialize Admin SalesPerson:
        SalesPerson salesPerson = business.getSalesPersonDirectory().addNewSalesPerson();
        salesPerson.setSalesPersonName("admin");
        salesPerson.setSaleProfits(0);
        salesPerson.setSaleQuantity(0);
        
        
////// Initialize Admin Account:
        UserAccount userAccount = business.getUserAccountDirectory().addNewUserAccount();
        userAccount.setUsername("admin");
        userAccount.setPassword("admin");
        userAccount.setIsActive(true);
        userAccount.setRole(userAccount.ADMIN_ROLE);
        userAccount.setSalesPerson(salesPerson);
        
////// Initialize Sales Person: total of 3 people
        SalesPerson sp1 = business.getSalesPersonDirectory().addNewSalesPerson();
        sp1.setSalesPersonName("Sales Person A");
        
        SalesPerson sp2 = business.getSalesPersonDirectory().addNewSalesPerson();
        sp2.setSalesPersonName("Sales Person B");
        
        SalesPerson sp3 = business.getSalesPersonDirectory().addNewSalesPerson();
        sp3.setSalesPersonName("Sales Person C");
        
////// Initialize User Account: total of 3 accounts
        UserAccount ua1 = business.getUserAccountDirectory().addNewUserAccount();
        ua1.setUsername("spA");
        ua1.setPassword("spA");
        ua1.setIsActive(true);
        ua1.setRole(UserAccount.SalesPerson_ROLE);
        ua1.setSalesPerson(sp1);
        
        UserAccount ua2 = business.getUserAccountDirectory().addNewUserAccount();
        ua2.setUsername("spB");
        ua2.setPassword("spB");
        ua2.setIsActive(true);
        ua2.setRole(UserAccount.SalesPerson_ROLE);
        ua2.setSalesPerson(sp2);
        
        UserAccount ua3 = business.getUserAccountDirectory().addNewUserAccount();
        ua3.setUsername("spC");
        ua3.setPassword("spC");
        ua3.setIsActive(true);
        ua3.setRole(UserAccount.SalesPerson_ROLE);
        ua3.setSalesPerson(sp3);
        
////// Initialize Customer: total of 3 customers
        Customer c1 = business.getCustomerDirectory().addNewCustomer();
        c1.setCustomerName("C1");
        c1.setContactInfo("C1 Info");
        
        Customer c2 = business.getCustomerDirectory().addNewCustomer();
        c2.setCustomerName("C2");
        c2.setContactInfo("C2 Info");
        
        Customer c3 = business.getCustomerDirectory().addNewCustomer();
        c3.setCustomerName("C3");
        c3.setContactInfo("C3 Info");
        
        return business;
    }
}
