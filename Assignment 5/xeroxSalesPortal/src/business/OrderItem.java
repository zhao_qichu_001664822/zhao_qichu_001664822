/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author ZappyZhao
 */
public class OrderItem {
    private Product product;
    private int orderQuantity;
    private float salePrice;
    private float interest;
    private boolean aboveFlag;
    private boolean belowFlag;

    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(float salePrice) {
        this.salePrice = salePrice;
    }

    public float getInterest() {
        return interest;
    }

    public void setInterest(float interest) {
        this.interest = interest;
    }

    public boolean isAboveFlag() {
        return aboveFlag;
    }

    public void setAboveFlag(boolean aboveFlag) {
        this.aboveFlag = aboveFlag;
    }

    public boolean isBelowFlag() {
        return belowFlag;
    }

    public void setBelowFlag(boolean belowFlag) {
        this.belowFlag = belowFlag;
    }

    @Override
    public String toString() {
        return this.getProduct().getProductName();
    }
    
    
    
}
