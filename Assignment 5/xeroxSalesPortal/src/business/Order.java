/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author ZappyZhao
 */
public class Order {
    private ArrayList<OrderItem> listOfItems;
    private static int count = 0;
    private int orderNumber;

    public Order() {
        listOfItems = new ArrayList<OrderItem>();
        orderNumber = ++count;        
    }

    public ArrayList<OrderItem> getListOfItems() {
        return listOfItems;
    }

    public void setListOfItems(ArrayList<OrderItem> listOfItems) {
        this.listOfItems = listOfItems;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }
    
//////// Add items:
    public OrderItem addNewItems() {
        OrderItem oi = new OrderItem();
        listOfItems.add(oi);
        return oi;
    }
/////// Delete item:
    public void deleteItem(OrderItem oi) {
        listOfItems.remove(oi);
    }
}
