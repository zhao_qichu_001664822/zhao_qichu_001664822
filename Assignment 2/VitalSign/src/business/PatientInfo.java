/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author ZappyZhao
 */
public class PatientInfo {
    private String patientName;
    private String patientID;
    private int patientAge;
    private String patientDoctor;
    private String patientPharmacy;
    
    
    public PatientInfo(){
        
    }
    
    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public int getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(int patientAge) {
        this.patientAge = patientAge;
    }

    public String getPatientDoctor() {
        return patientDoctor;
    }

    public void setPatientDoctor(String patientDoctor) {
        this.patientDoctor = patientDoctor;
    }

    public String getPatientPharmacy() {
        return patientPharmacy;
    }

    public void setPatientPharmacy(String patientPharmacy) {
        this.patientPharmacy = patientPharmacy;
    }


    
}
