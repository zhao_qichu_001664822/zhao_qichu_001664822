/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donor;

import Business.Employee.Employee;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author ZappyZhao
 */
public class Donor extends Employee{
    private ArrayList<Record> recordsList;

    public Donor() {
        super();
        recordsList = new ArrayList<>();
    
    }
      
    
    public ArrayList<Record> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(ArrayList<Record> recordsList) {
        this.recordsList = recordsList;
    }

    public Record addRecord(String bloodType, Date date, String barcode) {
        Record record = new Record();
        record.setBloodType(bloodType);
        record.setDate(date);
        record.setBarcode(barcode);
        recordsList.add(record);
        return record;
    }

    
}
